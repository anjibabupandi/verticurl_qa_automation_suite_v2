package org.verticurl.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

public class ReadExcelFileDemo {
	XLUtils xls;

	@Test
	private void readFile() throws Exception {
		xls = new XLUtils();
		String s=xls.getCellData("C:\\Users\\Verticurl-Users\\Documents\\BoT\\TEST_ACTIONS.xlsx", "TEST_CASES", 1, 5);
		List<String> list = Arrays.asList(s.split("\\s*,\\s*"));
		
		for(String str:list) {
		System.out.println(str);
		}

	}
}
