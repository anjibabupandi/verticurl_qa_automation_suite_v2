package org.verticurl.qa.utilities;

import java.io.File;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.verticurl.qa.dependent.TestConfiguration;
import org.yaml.snakeyaml.reader.UnicodeReader;

public class DynamicTestNG {

	public void runTestNGTest(List testngParams) { // Create an instance on TestNG
		TestNG myTestNG = new TestNG();

		// Create an instance of XML Suite and assign a name for it.
		XmlSuite mySuite = new XmlSuite();
		mySuite.setName("MySuite");
		mySuite.setParallel(XmlSuite.ParallelMode.METHODS);
		Map<String, String> params;
//======================================test1==============================
		for(int i=1;i<=testngParams.size();i++) {
		if(i==1) {
		// Create an instance of XmlTest and assign a name for it.
		XmlTest myTest = new XmlTest(mySuite);
		myTest.setName("MyTest");

		// Add any parameters that you want to set to the Test.
		myTest.addParameter("browser",testngParams.get(i-1).toString());
		

		// Create a list which can contain the classes that you want to run.
		List<XmlClass> myClasses = new ArrayList<XmlClass>();
		myClasses.add(new XmlClass("org.verticurl.qa.utilities.ReadTestCases"));

		// Assign that to the XmlTest Object created earlier.
		myTest.setXmlClasses(myClasses);

		// Create a list of XmlTests and add the Xmltest you created earlier to it.
		List<XmlTest> myTests = new ArrayList<XmlTest>();
		myTests.add(myTest);

		// add the list of tests to your Suite.
		mySuite.setTests(myTests);
		myTest.getAllParameters();
		}
		//----------------------------------------test2------------------------------
		if(i==2) {
		XmlTest myTest1 = new XmlTest(mySuite);
		myTest1.setName("MyTest1");

		// Add any parameters that you want to set to the Test.
		myTest1.addParameter("browser",testngParams.get(i-1).toString());
		

		// Create a list which can contain the classes that you want to run.
		List<XmlClass> myClasses1 = new ArrayList<XmlClass>();
		myClasses1.add(new XmlClass("org.verticurl.qa.utilities.ReadTestCases"));

		// Assign that to the XmlTest Object created earlier.
		myTest1.setXmlClasses(myClasses1);

		// Create a list of XmlTests and add the Xmltest you created earlier to it.
		List<XmlTest> myTests1 = new ArrayList<XmlTest>();
		myTests1.add(myTest1);
		myTest1.getAllParameters();
		
		}
		if(i==3) {
	XmlTest myTest2 = new XmlTest(mySuite);
		myTest2.setName("MyTest2");

		// Add any parameters that you want to set to the Test.
		myTest2.addParameter("browser",testngParams.get(i-1).toString());
		

		// Create a list which can contain the classes that you want to run.
		List<XmlClass> myClasses2 = new ArrayList<XmlClass>();
		myClasses2.add(new XmlClass("org.verticurl.qa.utilities.ReadTestCases"));

		// Assign that to the XmlTest Object created earlier.
		myTest2.setXmlClasses(myClasses2);

		// Create a list of XmlTests and add the Xmltest you created earlier to it.
		List<XmlTest> myTests2 = new ArrayList<XmlTest>();
		myTests2.add(myTest2);
		myTest2.getAllParameters();
		
		}
	}
		//---------------------------------------------------------------------

		// Add the suite to the list of suites.
		List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
		mySuites.add(mySuite);

		// Set the list of Suites to the testNG object you created earlier.
		myTestNG.setXmlSuites(mySuites);
		mySuite.setFileName("myTemp.xml");
		mySuite.setThreadCount(3);
		myTestNG.run();

		// Create physical XML file based on the virtual XML content
		for (XmlSuite suite : mySuites) {
			createXmlFile(suite);
		}
		System.out.println("Filerated successfully.");

		// Print the parameter values
		 

	}

	// This method will create an Xml file based on the XmlSuite data
	public void createXmlFile(XmlSuite mSuite) {
		FileWriter writer;
		try {
			writer = new FileWriter(new File("./testng.xml"));
			writer.write(mSuite.toXml());
			writer.flush();
			writer.close();
			System.out.println(new File("myTemp.xml").getAbsolutePath());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Main Method
	public static void main(String args[]) throws IOException {
		DynamicTestNG dt = new DynamicTestNG();
		XLUtils xls = new XLUtils();
		File creatDir = new File("C:\\TestData\\Files");
		String actionsPath = creatDir + "\\" + TestConfiguration.xlsx_Actions;
		String browser = xls.getCellData(actionsPath, "TEST_CASES",1, 5);
		List<String> browserList = Arrays.asList(browser.split("\\s*,\\s*"));
		dt.runTestNGTest(browserList);
	}
}
