package org.verticurl.qa.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class ReadConfig {

	String configPath = "";
	Properties prop = null;
	Properties prop1 = null;
	private String trackName;
	File file = new File("./Configurations/inputConfig.properties");
	File file1 = new File("./Configurations/outputConfig.properties");

	private static final Logger logger = Logger.getLogger(ReadConfig.class);

	public ReadConfig() {

		try {
			FileInputStream fis = new FileInputStream(file);

			prop = new Properties();

			prop.load(fis);

		} catch (Exception e) {
			logger.info("Exception is" + e.getMessage());

		}
	}


	public String getAppURL() {
		return prop.getProperty("baseURL");

	}

	public String getUserName() {
		return prop.getProperty("userName");
	}

	public String getPassword() {
		return prop.getProperty("password");
	}

	public String getChromePath() {
		return prop.getProperty("chromedriverpath");
	}

	public String getFirefoxPath() {
		return prop.getProperty("ffdriverpath");
	}

	public String getIEPath() {
		return prop.getProperty("iedriverpath");
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getSSPath() {
		return prop.getProperty("sspath");
	}

	public String getReportsPath() {
		return prop.getProperty("reports");
	}

	public String getValueFromResource(String val) {

		try {
			FileInputStream fis = new FileInputStream(file);
			prop = new Properties();
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty(val);
	}

	public void setValueToResource(String key, String value) throws ConfigurationException {
		
		PropertiesConfiguration conf = new PropertiesConfiguration(file1);
		conf.setProperty(key, value);
		conf.save();

	}

	public String getValueFromResourceOutPut(String val) throws IOException {

		FileInputStream fis = new FileInputStream(file1);
		prop = new Properties();
		prop.load(fis);

		return prop.getProperty(val);
	}

}
