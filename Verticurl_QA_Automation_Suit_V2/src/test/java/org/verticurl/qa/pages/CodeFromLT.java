package org.verticurl.qa.pages;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CodeFromLT {
    String username1 = "anjibabu.pandi";
    String accesskey = "qlHlcwGxhQsYryRYMrrbkYUdtfrLf5li2dnvZ59ytqn7nRsX3h";
    static RemoteWebDriver driver = null;
    String gridURL = "@hub.lambdatest.com/wd/hub";
    boolean status = false;
    
    @BeforeTest
    public void setup(Method m, ITestContext ctx) throws MalformedURLException {
    	String username = System.getenv("LT_USERNAME") == null ? username1 : System.getenv("LT_USERNAME");
        String authkey = System.getenv("LT_ACCESS_KEY") == null ? accesskey : System.getenv("LT_ACCESS_KEY");
        String hub = "@hub.lambdatest.com/wd/hub";

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platform", "MacOS Catalina");
        caps.setCapability("browserName", "Safari");
        caps.setCapability("version", "latest");
        caps.setCapability("build", "TestNG With Java");
        caps.setCapability("name", m.getName() + " - " + this.getClass().getName());
        caps.setCapability("plugin", "git-testng");

        String[] Tags = new String[] { "Feature", "Falcon", "Severe" };
        caps.setCapability("tags", Tags);

        driver = new RemoteWebDriver(new URL("https://" + username + ":" + authkey + hub), caps);

    }
	@Test
	public void testSimple() throws Exception {
		try {// Change it to production page
			driver.get("https://www.linkedin.com");
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			// Let's mark done first two items in the list.
			driver.findElement(By.id("session_key")).sendKeys("anjibabu");
			driver.findElement(By.id("session_password")).sendKeys("password");
			// Let's add an item in the list.
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.findElement(By.xpath("(//button[contains(.,'Sign in')])[1]")).click();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

  
    @AfterTest
    private void tearDown() {
        if (driver != null) {
            ((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
            driver.quit(); //really important statement for preventing your test execution from a timeout.
        }
    }
}
