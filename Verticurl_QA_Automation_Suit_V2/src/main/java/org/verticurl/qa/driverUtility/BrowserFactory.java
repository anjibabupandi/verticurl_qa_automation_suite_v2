package org.verticurl.qa.driverUtility;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.ITestContext;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.logger.LoggerUtils;

public class BrowserFactory {

	public static WebDriver driver = null;
	boolean status = false;

	public static WebDriver startApplication(String browser, boolean isLocal) throws FileNotFoundException {

		// driver open
		if (browser.equalsIgnoreCase("chrome")) {
			if (isLocal) {
				driver = DriverFactory.getDriver(DriverType.CHROME);
			} else {
				ChromeOptions browserOptions = new ChromeOptions();
				browserOptions.setPlatformName("Windows 10");
				browserOptions.setBrowserVersion("latest");
				browserOptions.setCapability("LT:Options", CapabilitySupplier.webCapabalitySet(browser));
				LoggerUtils.info("Capabilitites - chrome" + browserOptions);

				try {
					driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
							+ TestConfiguration.accessKey + TestConfiguration.gridURLWeb), browserOptions);
				} catch (MalformedURLException e) {
					System.out.println("Invalid grid URL");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			return driver;

		} else if (browser.equalsIgnoreCase("firefox")) {
			if (isLocal) {
				driver = DriverFactory.getDriver(DriverType.FIREFOX);
			} else {
				FirefoxOptions browserOptions = new FirefoxOptions();
				browserOptions.setPlatformName("Windows 10");
				browserOptions.setBrowserVersion("latest");
				browserOptions.setCapability("LT:Options", CapabilitySupplier.webCapabalitySet(browser));
				LoggerUtils.info("Capabilitites - firefox" + browserOptions);

				try {
					driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
							+ TestConfiguration.accessKey + TestConfiguration.gridURLWeb), browserOptions);
				} catch (MalformedURLException e) {
					System.out.println("Invalid grid URL");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			return driver;

		} else if (browser.equalsIgnoreCase("edge")) {
			if (isLocal) {
				driver = DriverFactory.getDriver(DriverType.Edge);
			} else {
				EdgeOptions browserOptions = new EdgeOptions();
				browserOptions.setPlatformName("Windows 10");
				browserOptions.setBrowserVersion("latest");
				browserOptions.setCapability("LT:Options", CapabilitySupplier.webCapabalitySet(browser));
				LoggerUtils.info("Capabilitites - edge" + browserOptions);

				try {
					driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
							+ TestConfiguration.accessKey + TestConfiguration.gridURLWeb), browserOptions);
				} catch (MalformedURLException e) {
					System.out.println("Invalid grid URL");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			}

			return driver;

		} else if (browser.equalsIgnoreCase("safari")) {

			SafariOptions browserOptions = new SafariOptions();
			browserOptions.setPlatformName("MacOS Ventura");
			browserOptions.setBrowserVersion("latest");

			browserOptions.setCapability("LT:Options", CapabilitySupplier.webCapabalitySet(browser));
			LoggerUtils.info("Capabilitites - safari" + browserOptions);

			try {
				driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
						+ TestConfiguration.accessKey + TestConfiguration.gridURLWeb), browserOptions);
			} catch (MalformedURLException e) {
				System.out.println("Invalid grid URL");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			return driver;

		} else {
			try {
				LoggerUtils.info("No Browser driver found");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return driver;
		}

	}

	public String getSuiteDetails(ITestContext ctx) {
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		return suiteName;
	}
}
