package org.verticurl.qa.driverUtility;

public enum DriverType {
	CHROME,
    FIREFOX,
    SAFARI,
    IE,
    Edge;
}
