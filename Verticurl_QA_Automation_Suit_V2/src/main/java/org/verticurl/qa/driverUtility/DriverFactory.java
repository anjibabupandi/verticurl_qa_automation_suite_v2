package org.verticurl.qa.driverUtility;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.Architecture;

public class DriverFactory {

	DriverFactory() {

	}

	private static final Map<DriverType, Supplier<WebDriver>> driverMap = new HashMap<>();

	// chrome driver supplier
	private static final Supplier<WebDriver> chromeDriverSupplier = () -> {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.setPageLoadStrategy(PageLoadStrategy.NONE);
		return new ChromeDriver(options);
	};

	// firefox driver supplier
	private static final Supplier<WebDriver> firefoxDriverSupplier = () -> {
		FirefoxOptions options = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.dir", FileUtils.getTempDirectory().toString() + "/firefox/");
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", true);
		profile.setPreference("app.update.auto", false);
		profile.setPreference("app.update.enabled", false);
		profile.setPreference("dom.max_script_run_time", 0);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;text/json; charset=UTF-8");
		profile.setPreference("pdfjs.disabled", true);

		options.setProfile(profile);
		options.setLogLevel(FirefoxDriverLogLevel.TRACE);
		options.setCapability("marionette", true);
		options.setCapability("Platform", org.openqa.selenium.Platform.ANY);
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");

		WebDriverManager.firefoxdriver().setup();
		return new FirefoxDriver(options);
	};

	// firefox driver supplier
	private static final Supplier<WebDriver> internetExplorerSupplier = () -> {
		WebDriverManager.iedriver().architecture(Architecture.X32).arch32().setup();
		return new InternetExplorerDriver();
	};
	
	private static final Supplier<WebDriver> edgeBrowserSupplier = () -> {
		WebDriverManager.edgedriver().setup();
		return new EdgeDriver();
	};

	static {
		driverMap.put(DriverType.CHROME, chromeDriverSupplier);
		driverMap.put(DriverType.FIREFOX, firefoxDriverSupplier);
		driverMap.put(DriverType.IE, internetExplorerSupplier);
		driverMap.put(DriverType.Edge, edgeBrowserSupplier);

	}

	// return a new driver from the map
	public static final WebDriver getDriver(DriverType type) {
		return driverMap.get(type).get();
	}
}
