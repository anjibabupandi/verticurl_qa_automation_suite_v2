package org.verticurl.qa.driverUtility;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import org.verticurl.mobile.dependent.MobileDeviceDetails;

public class CapabilitySupplier {

	static MobileDeviceDetails md;
	
	public static HashMap webCapabalitySet(String browser) {
		
		HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		ltOptions.put("build", "Verticurl_QA_Automation_Build");
		ltOptions.put("project", "Verticurl_QA_Automation");
		ltOptions.put("name", "Verticurl_Test_Automation-"+browser.toUpperCase());
		ltOptions.put("selenium_version", "4.0.0");
		ltOptions.put("w3c", true);
		return ltOptions;
	}
	
public static HashMap mobileCapabalitySet(String browser) {
		md =new MobileDeviceDetails();
		List deviceList=md.getDeviceDetails(browser);
		HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		ltOptions.put("build", "Verticurl_QA_Automation_Build");
		ltOptions.put("project", "Verticurl_QA_Automation");
		ltOptions.put("name", "Verticurl_Test_Automation-"+browser.toUpperCase());
		ltOptions.put("selenium_version", "4.0.0");
		ltOptions.put("w3c", true);
		ltOptions.put("deviceName",deviceList.get(0));
		ltOptions.put("platformName", deviceList.get(1));
		ltOptions.put("platformVersion", deviceList.get(2));
		ltOptions.put("isRealMobile", true);
		ltOptions.put("visual", true);
		ltOptions.put("network", true);
		ltOptions.put("video", true);
		ltOptions.put("console", true);

		return ltOptions;
	}

	static String deviceType;

	public static String setDeviceType(String browser) throws FileNotFoundException {
		if(browser.equalsIgnoreCase("chrome") || browser.equalsIgnoreCase("firefox") || browser.equalsIgnoreCase("edge") ||  browser.equalsIgnoreCase("safari")) {
			deviceType = "windows";
			return deviceType;
		}else if(browser.equalsIgnoreCase("android")) {
			deviceType = "android";
			return deviceType;
		}else if(browser.equalsIgnoreCase("ios")) {
			deviceType = "ios";
			return deviceType;
		}else {
			deviceType = "test";
			return deviceType;
		}

	}
}
