package org.verticurl.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.dependent.KeywordsEngine;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.logger.LoggerUtils;

import io.qameta.allure.Step;

public class ReadActionsSuite {
	XLUtils xls;
	List<String> actions_TCID;
	List<String> actions_TCStep;
	List<String> actions_ORRef;
	List<String> actions_OREle;
	List<String> actions_ActName;
	List<String> actions_ActData;
	ReadObjectRepository readObjRepo;
	KeywordsEngine ke;
	protected static WebDriver driver;
	public static Method method;
	File file = new File("./Allure-Files/environment.properties");
	String URL;
	String Env;

	public static DataFormatter dataFormatter;
	public static Workbook workbook = null;
	public static FormulaEvaluator formulaEvaluator;
	public static Sheet sheet;
	FileInputStream fis;
	GenericUtilities ge;

	@Step("Current Step is {testCaseID}")
	public void readTestActionsData(String actionsSheet, String testCaseID, String browser) throws Exception {

		LoggerUtils.info("readTestActionsData method STARTED");
		String actionaFilePath = BaseClass.actionsFilePath();
		String objectRepoFilePath = BaseClass.orFilePath();
		LoggerUtils.info("readTestActionsData actionaFilePath " + actionaFilePath);
		LoggerUtils.info("readTestActionsData objectRepoFilePath " + objectRepoFilePath);

		ge = new GenericUtilities();
		xls = new XLUtils();
		readObjRepo = new ReadObjectRepository();
		ke = new KeywordsEngine();
		try {

			actions_TCID = new ArrayList<String>();
			actions_TCStep = new ArrayList<String>();
			actions_ORRef = new ArrayList<String>();
			actions_OREle = new ArrayList<String>();
			actions_ActName = new ArrayList<String>();
			actions_ActData = new ArrayList<String>();

			String cellValue_TCID;
			String cellValue_TCStep;
			String cellValue_ORRef;
			String cellValue_OREle;
			String cellValue_ActName;
			String cellValue_ActData;

			dataFormatter = new DataFormatter();
			fis = new FileInputStream(actionaFilePath);
			workbook = WorkbookFactory.create(fis);
			formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
			sheet = workbook.getSheet(actionsSheet);

			for (Row row : sheet) { // iterate over all rows in the sheet
				Cell act_TCID = row.getCell(TestConfiguration.act_Actions_TCID);
				Cell act_TCStep = row.getCell(TestConfiguration.act_Actions_TCStep);
				Cell act_ORRef = row.getCell(TestConfiguration.act_Actions_ORRef);
				Cell act_OREle = row.getCell(TestConfiguration.act_Actions_OREle);
				Cell act_ActName = row.getCell(TestConfiguration.act_Actions_ActionName);
				Cell act_ActData = row.getCell(TestConfiguration.act_Actions_ActionData);

				if (act_TCID != null && act_TCStep != null && act_ActName != null) {
					cellValue_TCID = dataFormatter.formatCellValue(act_TCID, formulaEvaluator);
					cellValue_TCStep = dataFormatter.formatCellValue(act_TCStep, formulaEvaluator);
					cellValue_ORRef = dataFormatter.formatCellValue(act_ORRef, formulaEvaluator);
					cellValue_OREle = dataFormatter.formatCellValue(act_OREle, formulaEvaluator);
					cellValue_ActName = dataFormatter.formatCellValue(act_ActName, formulaEvaluator);
					cellValue_ActData = dataFormatter.formatCellValue(act_ActData, formulaEvaluator);

					actions_TCID.add(cellValue_TCID.trim().toString()); // add that row to the results
					actions_TCStep.add(cellValue_TCStep.trim().toString()); // add that row to the results
					actions_ORRef.add(cellValue_ORRef.trim().toString()); // add that row to the results
					actions_OREle.add(cellValue_OREle.trim().toString()); // add that row to the results
					actions_ActName.add(cellValue_ActName.trim().toString()); // add that row to the results
					actions_ActData.add(cellValue_ActData.trim().toString()); // add that row to the results

				}

			}

			String OrElementName;

			LoggerUtils.info("actions_TCID Size ==>" + actions_TCID.size());

			for (int i = 1; i < actions_TCID.size(); i++) {
				cellValue_TCID = xls.readCellContent(actionaFilePath, actionsSheet, actions_TCID.get(i).toString(),
						TestConfiguration.act_Actions_TCID);
				cellValue_TCStep = xls.readCellContent(actionaFilePath, actionsSheet, actions_TCStep.get(i).toString(),
						TestConfiguration.act_Actions_TCStep);
				cellValue_ORRef = xls.readCellContent(actionaFilePath, actionsSheet, actions_ORRef.get(i).toString(),
						TestConfiguration.act_Actions_ORRef);
				cellValue_OREle = xls.readCellContent(actionaFilePath, actionsSheet, actions_OREle.get(i).toString(),
						TestConfiguration.act_Actions_OREle);
				cellValue_ActName = xls.readCellContent(actionaFilePath, actionsSheet,
						actions_ActName.get(i).toString(), TestConfiguration.act_Actions_ActionName);
				cellValue_ActData = xls.readCellContent(actionaFilePath, actionsSheet,
						actions_ActData.get(i).toString(), TestConfiguration.act_Actions_ActionData);

				// Reflection Start
				if (actions_TCID.get(i).toString().equals(testCaseID)) {

					cellValue_TCID = actions_TCID.get(i).toString();
					cellValue_TCStep = actions_TCStep.get(i).toString();
					cellValue_ORRef = actions_ORRef.get(i).toString();
					cellValue_OREle = actions_OREle.get(i).toString();
					cellValue_ActName = actions_ActName.get(i).toString();
					cellValue_ActData = actions_ActData.get(i).toString();

					LoggerUtils.info("cellValue_TCID\t==>" + cellValue_TCID);
					LoggerUtils.info("cellValue_TCStep\t==>" + cellValue_TCStep);
					LoggerUtils.info("cellValue_ORRef\t==>" + cellValue_ORRef);
					LoggerUtils.info("cellValue_OREle\t==>" + cellValue_OREle);
					LoggerUtils.info("cellValue_ActName\t==>" + cellValue_ActName);
					LoggerUtils.info("cellValue_ActData\t==>" + cellValue_ActData);

					// This block is for launchURL
					if (cellValue_ActName.equals("launchURL")) {
						driver = BaseClass.initWebDriver(browser, cellValue_ActData.trim());
						LoggerUtils.info("driver===================" + driver.getTitle());

						URL = cellValue_ActData.trim();

					} else if (cellValue_ActName.equals("launchURLAuth")) {
						String[] inputData = cellValue_ActData.split("\\,");

						if (inputData.length > 1) {
							LoggerUtils.info("Size" + inputData.length);
							LoggerUtils.info("URL" + inputData[0]);
							LoggerUtils.info("UserName" + inputData[1]);
							LoggerUtils.info("Password" + inputData[2]);
							driver = BaseClass.initWebDriver(browser, inputData[0].trim());
							ge.typeUser(inputData[1]);
							ge.typePass(inputData[2]);
							ge.pressEnter();
						} else {
							driver = BaseClass.initWebDriver(browser, inputData[0].trim());

						}

					}
					// This block is for all the methods in KeywordsEngine
					else {
						List<Object> myParamList = new ArrayList<Object>();
						// Adding driver to myParamList
						myParamList.add(driver);
						LoggerUtils.info("Driver is" + driver);
						// Adding Object value to myParamList
						if (!cellValue_ORRef.isEmpty() && !cellValue_ORRef.equals("NA") && !cellValue_OREle.isEmpty()
								&& !cellValue_OREle.equals("NA")) {
							OrElementName = readObjRepo.readObejectRepoData(objectRepoFilePath, cellValue_ORRef.trim(),
									cellValue_OREle.trim());
							myParamList.add(OrElementName.trim());
							LoggerUtils.info("OR Sheet Loc Name==>" + cellValue_ORRef);
							LoggerUtils.info("OR Sheet Loc Value==>" + cellValue_OREle);

						}
						// Adding testData to myParamList
						if (!cellValue_ActData.isEmpty() & !cellValue_ActData.equals("NA")) {
							myParamList.add(cellValue_ActData.trim());
						}
						// Creating Object
						Object[] paramListObject = new Object[myParamList.size()];
						paramListObject = myParamList.toArray(paramListObject);
						runReflectionMethod(cellValue_ActName, paramListObject);
					} // else
				} // If Reflection End
			}
			// }
			actions_TCID.clear();// add that row to the results
			actions_TCStep.clear(); // add that row to the results
			actions_ORRef.clear(); // add that row to the results
			actions_OREle.clear(); // add that row to the results
			actions_ActName.clear(); // add that row to the results
			actions_ActData.clear();
			LoggerUtils.info("readTestActionsData method END");

			fis.close();
			FileOutputStream fileOut = new FileOutputStream(BaseClass.actionsFilePath());
			workbook.write(fileOut);
			fileOut.close();

			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());

		}

	}

	public void runReflectionMethod(String strMethodName, Object... inputArgs) throws Exception {

		// params is for method signature
		Class<?> params[] = new Class[inputArgs.length];
		// First parameter is for driver object
		params[0] = WebDriver.class;
		// ObjectRepo & testData
		for (int i = 1; i < inputArgs.length; i++) {
			params[i] = inputArgs[i].getClass();
		}
		// Get method from KeywordsEngine
		method = ke.getClass().getDeclaredMethod(strMethodName, params);
		// invoke the method by using actual values(inputArgs)
		method.invoke(ke, inputArgs);

	}
}