package org.verticurl.qa.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.verticurl.qa.logger.LoggerUtils;

public class XLUtils {

	private FileInputStream fis;
	private FileOutputStream fos;
	private Workbook wb;
	private Sheet sh;
	private Cell cell;
	private Row row;
	String sJson;

	public int getRowCount(String xlFile, String xlSheet) throws IOException {

		int rowCount = 0;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			rowCount = sh.getLastRowNum();

			wb.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return rowCount;

	}

	public int getCellCount(String xlFile, String xlSheet, int rowNum) throws IOException {

		int cellCount = 0;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			row = sh.getRow(rowNum);

			cellCount = row.getLastCellNum();

			wb.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return cellCount;

	}

	public String getCellData(String xlFile, String xlSheet, int rowNum, int colNum) throws IOException {
		String cellData = null;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			row = sh.getRow(rowNum);
			cell = row.getCell(colNum);

			DataFormatter formatter = new DataFormatter();
			cellData = formatter.formatCellValue(cell);
			return cellData;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			cellData = "";
		}

		wb.close();
		fis.close();

		return cellData;

	}

	public void setCellData(String xlFile, String xlSheet, int rowNum, int colNum, String data) throws IOException {
		LoggerUtils.info("xlFile" + xlFile);
		LoggerUtils.info("xlSheet" + xlSheet);
		LoggerUtils.info("rowNum" + rowNum);
		LoggerUtils.info("colNum" + colNum);
		LoggerUtils.info("data" + data);

		fis = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fis);
		sh = wb.getSheet(xlSheet);
		row = sh.getRow(rowNum);
		LoggerUtils.info("Row" + row.getRowNum());

		if (row == null) {
			row = sh.createRow(rowNum);
		}
		cell = row.getCell(colNum);
		if (cell == null) {
			cell = row.createCell(colNum);
		}
		cell.setCellValue(data);
		LoggerUtils.info("Cell" + cell.getStringCellValue());

		fis.close();
		fos = new FileOutputStream(xlFile);
		wb.write(fos);
		fos.close();

	}

	public void setCellData_1(String xlFile, Sheet xlSheet, int rowNum, int colNum, String data) throws Exception {
		LoggerUtils.info("xlFile" + xlFile);
		LoggerUtils.info("xlSheet" + xlSheet.getSheetName());
		LoggerUtils.info("rowNum" + rowNum);
		LoggerUtils.info("colNum" + colNum);
		LoggerUtils.info("data" + data);

		row = xlSheet.getRow(rowNum);
		LoggerUtils.info("Row" + row.getRowNum());

		if (row == null) {
			row = xlSheet.createRow(rowNum);
		}
		cell = row.getCell(colNum);
		if (cell == null) {
			cell = row.createCell(colNum);
		}
		cell.setAsActiveCell();
		cell.setCellValue(data);
		TimeUnit.SECONDS.sleep(2);
		LoggerUtils.info("Cell" + cell.getStringCellValue());

	}

	public String readCellContent(String excelPath, String sheet, String locatorName, int colNo) throws Exception {
		String content = null;
		if (!locatorName.isEmpty() && !(locatorName == "NA")) {
			fis = new FileInputStream(excelPath);
			wb = WorkbookFactory.create(fis);
			sh = wb.getSheet(sheet);
			int rownr = findRow(sh, locatorName);
			content = output(sh, rownr, colNo);
		} else {
			content = "No Values";
		}
		return content;
	}

	private String output(Sheet sheet, int rownr, int colnr) {
		DataFormatter formatter = new DataFormatter();
		row = sheet.getRow(rownr);
		cell = row.getCell(colnr);

		return formatter.formatCellValue(cell);
	}

	private int findRow(Sheet sheet, String cellContent) {
		DataFormatter formatter = new DataFormatter();

		int rowNum = 0;
		for (Row row : sheet) {
			for (Cell cell : row) {
				String celldata = formatter.formatCellValue(cell);

				if (celldata.equalsIgnoreCase(cellContent)) {
					rowNum = row.getRowNum();
					// LoggerUtils.info("rowNum"+rowNum);
				}

			}
		}
		return rowNum;
	}

	public boolean checkSheetExists(String ExcelPath, String sheet) throws Exception {
		boolean flag = false;

		LoggerUtils.info("sheet from Class" + sheet);
		try {
			fis = new FileInputStream(ExcelPath);
			wb = WorkbookFactory.create(fis);

			if (wb.getNumberOfSheets() != 0) {
				for (int i = 0; i < wb.getNumberOfSheets(); i++) {
					if (wb.getSheetName(i).equals(sheet)) {
						sh = wb.getSheet(sheet);
						flag = true;
					}
				}
			}
		} catch (Exception e) {
			LoggerUtils.info(e.getMessage());
		}
		return flag;
	}

	public int searchStringInXslx(String string, String filepath, String sheet) throws IOException {

		fis = new FileInputStream(filepath);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet(sheet);
		Iterator<Row> iterator = sh.iterator();
		int columnNumber = 0;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell.getCellType() == CellType.STRING) {
					String text = cell.getStringCellValue();
					if (string.equals(text)) {
						columnNumber = cell.getRowIndex();
						break;
					}
				}
			}
		}
		fis.close();
		// workbook.close();

		return columnNumber;
	}
}
