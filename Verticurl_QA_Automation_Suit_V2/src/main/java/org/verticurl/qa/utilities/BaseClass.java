package org.verticurl.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.verticurl.mobile.dependent.MobileBrowserFactory;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.driverUtility.BrowserFactory;
import org.verticurl.qa.driverUtility.CapabilitySupplier;
import org.verticurl.qa.logger.LoggerUtils;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Anjibabu-VIN1039
 *
 */
public class BaseClass {

	protected static WebDriver driver;
	static DataProviderClass dp;

	public static WebDriver initWebDriver(String browser, String appURL) throws FileNotFoundException {
		try {
			dp = new DataProviderClass();
				if (CapabilitySupplier.setDeviceType(browser).equals("windows")) {
					driver = BrowserFactory.startApplication(browser,dp.isLocal());

				} else if (CapabilitySupplier.setDeviceType(browser).equals("android")) {
					driver = MobileBrowserFactory.startApplicationMobile(browser);

				} else if (CapabilitySupplier.setDeviceType(browser).equals("ios")) {
					driver = MobileBrowserFactory.startApplicationMobile(browser);

				}
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.get(appURL);
				driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
				return driver;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	
	}

	@AfterSuite
	public void tearDown() throws Exception {
		if (driver != null) {
			((JavascriptExecutor) driver).executeScript("status=" + driver);
			driver.quit();
		}
	}

	static File creatDir = new File("C:\\TestData\\Files");
	static String actionsPath;
	static String orPath;

	@BeforeSuite
	public void loadAllExcelFiles() throws Exception {
		try {
			actionsFilePath();
			orFilePath();
		} catch (Exception e) {
			LoggerUtils.info(e.getMessage());
			LoggerUtils.error(e.getClass().getName());
			LoggerUtils.info("FILES ARE NOT EXISTING IN DIRECTORY, PLEASE KEEP FILES IN" + creatDir);

		}
	}

	public static String actionsFilePath() throws FileNotFoundException {

		String[] listOfFiles = creatDir.list();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].equals(TestConfiguration.xlsm_Actions)) {
				actionsPath = creatDir + "\\" + TestConfiguration.xlsm_Actions;
			} else {
				actionsPath = creatDir + "\\" + TestConfiguration.xlsx_Actions;
			}
		}
		return actionsPath;

	}

	public static String orFilePath() throws FileNotFoundException {

		String[] listOfFiles = creatDir.list();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].equals(TestConfiguration.path_OR)) {
				orPath = creatDir + "\\" + TestConfiguration.path_OR;
			} else {
				orPath = creatDir + "\\" + TestConfiguration.path_OR;
			}
		}
		return orPath;

	}

	public WebDriver getDriver() {
		return driver;
	}

	@AfterSuite
	public void onFinish() throws Exception {
		File src = null;
		File dest = null;
		try {

			src = new File("./Allure-Files\\");
			dest = new File("./allure-results\\");

			LoggerUtils.info("Source" + src);
			LoggerUtils.info("Destination" + dest);

			// FileUtils.copyDirectory(src, dest);
			// LoggerUtils.info("File has beed copied from " + src + " To Destination" +
			// dest);

			creatDir = new File("./target\\Allure-Files");
			if (!creatDir.exists()) {
				creatDir.mkdirs();
			}

			src = new File("./Allure-Files\\");
			dest = new File("./target\\Allure-Files");

			LoggerUtils.info("Source" + src);
			LoggerUtils.info("Destination" + dest);

			FileUtils.copyDirectory(src, dest);
			LoggerUtils.info("File has beed copied from " + src + " To Destination" + dest);

		} catch (Exception ex) {

			ex.printStackTrace();
		}

	}

	public static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {

		InputStream in = null;
		OutputStream out = null;
		try {
			if (sourceLocation.isDirectory()) {
				if (!targetLocation.exists()) {
					targetLocation.mkdir();
				}

				String[] children = sourceLocation.list();
				for (int i = 0; i < children.length; i++) {
					copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
				}
			} else {

				in = new FileInputStream(sourceLocation);
				out = new FileOutputStream(targetLocation);

				// Copy the bits from in stream to out stream
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			in.close();
			out.close();
		}
	}
}
