package org.verticurl.qa.utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class GenericUtilities {

	public static final Logger logger = Logger.getLogger(GenericUtilities.class);

	public void click(WebElement element) {
		// @vtiruvee --06/05/2018 added loop to click on the given element\
		for (int m = 0; m < 10; m++) {
			if (element != null) {
				element.click();
				break;
			}
		}
	}

	public void enterText(WebElement element, String val) {
		element.clear();
		element.sendKeys(val);
	}

	public void implecitWait(WebDriver driver, int waitTime) {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(waitTime));
	}

	public void selectText(WebElement element, String selectType) {

		String[] select = selectType.split("=");

		String type = select[0];
		String value = select[1];

		switch (type) {
		case "selectByVisibleText":
			new Select(element).selectByVisibleText(value);
			break;
		case "selectByValue":
			new Select(element).selectByValue(value);
			break;
		case "selectByIndex":
			new Select(element).selectByIndex(Integer.parseInt(value));
			break;

		}

	}

	public void explicitWaitClickable(WebDriver driver, WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
			if (element != null)
				wait.until(ExpectedConditions.elementToBeClickable(element));
			else {

				throw new Exception("Element is null");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void udfWaitUntillSpinOverlayDisappears(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(150));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		if (driver.findElements(By.xpath(".//div[@class='spinner']")).size() > 0) {
			for (int i = 0; i < driver.findElements(By.xpath(".//div[@class='spinner']")).size(); i++) {
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='spinner']")));
			}
		}

	}

	public void pageLoadTime(WebDriver driver) throws Exception {

		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(40));
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
		TimeUnit.SECONDS.sleep(2);
	}

	public void pageLoadTime60Seconds(WebDriver driver) {
		if (!checkIfcssSelectorExists(driver, "backdrop")) {
			(new WebDriverWait(driver, Duration.ofSeconds(80), Duration.ofSeconds(230)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("backdrop")));
		} else if (!checkIfXpathExists(driver, ".//*[@id='spinner-container']")) {
			(new WebDriverWait(driver, Duration.ofSeconds(80), Duration.ofSeconds(240)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.id("spinner-container")));
		}
	}

	public boolean checkIfcssSelectorExists(WebDriver driver, String cssSelectorName) {
		return driver.findElements(By.className(cssSelectorName)).isEmpty();
	}

	public boolean checkIfXpathExists(WebDriver driver, String xPath) {
		boolean foundXpath = true;
		for (int i = 0; i < 5; i++) {
			foundXpath = driver.findElements(By.xpath(xPath)).isEmpty();
			if (!foundXpath)
				break;
		}
		return foundXpath;
	}

	public boolean isAlertPresent(WebDriver driver) {
		try {
			WebDriverWait alertWait = new WebDriverWait(driver, Duration.ofSeconds(120));
			alertWait.until(ExpectedConditions.alertIsPresent());
			return true;
		} // try
		catch (Exception e) {
			return false;
		} // catch
	}

	public void clickOnWebElementUsingJavaScriptExecutor(WebDriver driver, WebElement element) {
		try {
			if (element.isEnabled() && element.isDisplayed()) {
				logger.info("Clicking on element " + element.getText() + "+with using java script click");
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(80));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				logger.info("Unable to click on element");
			}
		} catch (StaleElementReferenceException e) {
			logger.info("Element is not attached to the page document " + e.getStackTrace());
			if (element.isEnabled() && element.isDisplayed()) {
				logger.info("Clicking on element with using java script click");
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(120));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				logger.info("Unable to click on element");
			}
		} catch (NoSuchElementException e) {
			logger.info("Element was not found in DOM " + e.getStackTrace());
			if (element.isEnabled() && element.isDisplayed()) {
				logger.info("Clicking on element with using java script click");
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(120));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				logger.info("Unable to click on element");
			}
		} catch (Exception e) {
			logger.info("Unable to click on element " + e.getStackTrace());
		}
	}

	public void explicitWaitVisibility(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(80));
		if (element != null) {
			wait.until(ExpectedConditions.visibilityOf(element));
		} else {
			throw new WebDriverException("Element " + element + " is null");
		}
	}

	public void explicitWaitVisibilityPageLocated(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(80));
		if (element != null) {
			wait.until(ExpectedConditions.presenceOfElementLocated((By) element));
		} else {
			throw new WebDriverException("Element " + element + " is null");
		}
	}

	public void pageLoadTime30Seconds(WebDriver driver) {
		if (!checkIfcssSelectorExists(driver, "backdrop")) {

			(new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(230)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("backdrop")));
		} else if (!checkIfXpathExists(driver, ".//*[@id='spinner-container']")) {
			(new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(230)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.id("spinner-container")));
		}
	}

	public void pageLoadTime5Seconds(WebDriver driver) {
		try {
			if (!checkIfcssSelectorExists(driver, "backdrop")) {
				(new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(230)))
						.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("backdrop")));

				TimeUnit.SECONDS.sleep(2);

			} else if (!checkIfXpathExists(driver, ".//*[@id='spinner-container']")) {
				(new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(230)))
						.until(ExpectedConditions.invisibilityOfElementLocated(By.id("spinner-container")));
				TimeUnit.SECONDS.sleep(2);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void navigateToRequiredTab(WebDriver driver, WebElement ele) {

		explicitWaitVisibility(driver, ele);
		if (ele.isDisplayed()) {
			ele.click();
			pageLoadTime5Seconds(driver);
		}
	}

	public int generateRandomNumber4digi() {
		return new Random().nextInt(10000);
	}

	public String generateRandomChar() {
		// create a string of all characters
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		// create random string builder
		StringBuilder sb = new StringBuilder();

		// specify length of random string
		int length = 7;

		for (int i = 0; i < length; i++) {

			// generate random index number
			int index = new Random().nextInt(alphabet.length());

			// get character specified by index
			// from the string
			char randomChar = alphabet.charAt(index);

			// append the character to string builder
			sb.append(randomChar);
		}

		String randomString = sb.toString();
		logger.info("Random String is: " + randomString);

		return randomString;
	}

	public int switchWindows(WebDriver driver) {
		String parentWindow = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver.switchTo().window(windowHandle);
			}
		}
		return handles.size();
	}

	public static void setClipboardData(String string) {
		// StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile(String fileLocation) {
		try {
			setClipboardData(fileLocation);

			Robot robot = new Robot();
			TimeUnit.SECONDS.sleep(10);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	public String getRequestedMonths(int month) {
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
		c.add(Calendar.MONTH, month);
		String getMonth = sdf.format(c.getTime());

		return getMonth;
	}

	public void maximizeWindow(WebDriver driver) {
		driver.manage().window().maximize();
	}

	public void udfWaitUntillSpinOverlayDisappearsZprove(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(230));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
		if (driver.findElements(By.xpath(".//div[@class='spinner-bg']")).size() > 0) {
			for (int i = 0; i < driver.findElements(By.xpath(".//div[@class='spinner-bg']")).size(); i++) {
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='spinner-bg']")));
			}
		}

	}

	public boolean isDisplayed(WebElement element) {

		boolean blndisplay = false;
		if (element != null)
			blndisplay = element.isDisplayed();

		return blndisplay;
	}

	public void enterTextKey(WebElement element, String Val) {

		if (element.isEnabled())
			element.sendKeys(Val + Keys.RETURN);
	}

	public void webDriverWait(WebDriver driver, String strExpCondition, WebElement element, int intTimeOut) {
		switch (strExpCondition) {
		case "presenceOfElementLocated":

			break;
		case "invisibilityOfElementLocated":
			(new WebDriverWait(driver, Duration.ofSeconds(40)))
					.until(ExpectedConditions.invisibilityOfElementLocated((By) element));
			break;
		}
	}

	public boolean isSelected(WebElement element) {

		boolean blnSelect = false;
		if (element != null)
			blnSelect = element.isSelected();

		return blnSelect;
	}

	public boolean checkIfElementIdExists(WebDriver driver, String id) {
		boolean foundid = driver.findElements(By.id(id)).isEmpty();

		System.out.println("foundid =" + foundid);
		return foundid;

	}

	public String generateCurrentDateMMDDYYYYHHmmss() throws Exception {
		String strCurrentDate_MMddYYYYHHmmss = "";
		// SimpleDateFormat formatter = new
		// SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("MMddYYYYHHmmss");
		Calendar currentDate = Calendar.getInstance();
		strCurrentDate_MMddYYYYHHmmss = formatter.format(currentDate.getTime());
		// System.out.println("Current Date MMDDYYYYHHmmss = " +
		// strCurrentDate_MMddYYYYHHmmss);
		return strCurrentDate_MMddYYYYHHmmss;
	}

	public void pageLoadTime45Seconds(WebDriver driver) throws Exception {
		if (!checkIfcssSelectorExists(driver, "backdrop")) {
			(new WebDriverWait(driver, Duration.ofSeconds(40), Duration.ofSeconds(230)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("backdrop")));
		} else if (!checkIfXpathExists(driver, ".//*[@id='spinner-container']")) {
			(new WebDriverWait(driver, Duration.ofSeconds(40), Duration.ofSeconds(230)))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.id("spinner-container")));
		}
	}

	public boolean isClickable(WebDriver driver, WebElement inputWebElm, int timeMs) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeMs));
			wait.until(ExpectedConditions.elementToBeClickable(inputWebElm));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public boolean checkIfWebElementEnabled(WebElement inputWebElmnt) {
		boolean isWebElmntEnabled = false;
		try {
			if (inputWebElmnt.isEnabled()) {
				System.out.println("Element is Enabled" + inputWebElmnt.getText());
				isWebElmntEnabled = true;
			} else {
				isWebElmntEnabled = false;
				System.out.println("Element is Disabled" + inputWebElmnt.getText());
			}

		} catch (org.openqa.selenium.NoSuchElementException nse) {
			nse.printStackTrace();
		} catch (Exception exp) {
			exp.printStackTrace();
		}

		return isWebElmntEnabled;
	}

	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	Robot r;

	public void pasteClipboard() throws AWTException {
		r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.delay(50);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
	}

	public void typeUser(String text) throws AWTException {
		r = new Robot();
		writeToClipboard(text);
		r = new Robot();
		pasteClipboard();
	}

	public void typePass(String text) throws AWTException {
		r = new Robot();
		writeToClipboard(text);
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		pasteClipboard();

	}

	public void pressEnter() throws AWTException {
		r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
	}

	private void writeToClipboard(String s) {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable transferable = new StringSelection(s);
		clipboard.setContents(transferable, null);
	}
}