package org.verticurl.qa.utilities;

import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.verticurl.qa.Reporting.GenerateReports;
import org.verticurl.qa.logger.LoggerUtils;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Description;
import io.qameta.allure.Step;

@Listeners(GenerateReports.class)
public class ReadTestCases extends BaseClass {
	XLUtils xls;
	List<String> testCases_TSID;
	List<String> testCases_TCID;
	List<String> testCases_RunMode;
	List<String> testCases_TestType;

	ReadActionsSuite ractsuite;
	AllureLifecycle lifecycle = Allure.getLifecycle();

	String tc_Name;
	String ts_ID;
	String tc_ID;
	String tc_rm;
	String ts_type;
	String tc_Browser;

	@Parameters("browser")
	// @Test(dataProvider = "testCasesExecutor", dataProviderClass =
	// DataProviderClass.class)Map<String, String> map,
	@Step("Automation Execution Report")
	@Description("Executing Test Cases from Excel File")
	@Test
	public void readTestCasesData(String browser) throws Exception {
		LoggerUtils.info("readTestCasesData method STARTED");

		ractsuite = new ReadActionsSuite();
		try {

			FileInputStream file = new FileInputStream(actionsPath);
			XSSFWorkbook wb = new XSSFWorkbook(file);
			XSSFSheet sheet = wb.getSheet("TEST_CASES");

			int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
			for (int i = 1; i <= rowCount; i++) {
				// Enter the values read from Excel in firstname,lastname,mobile,email,address
				String testCaseName = sheet.getRow(i).getCell(0).getStringCellValue();
				String testSuiteID = sheet.getRow(i).getCell(1).getStringCellValue();
				String testCaseID = sheet.getRow(i).getCell(2).getStringCellValue();
				String runMode = sheet.getRow(i).getCell(3).getStringCellValue();
				String testType = sheet.getRow(i).getCell(4).getStringCellValue();
				// String browser = sheet.getRow(i).getCell(5).getStringCellValue();
				if (runMode.equalsIgnoreCase("Y")) {

					System.out.println(testCaseName);
					System.out.println(testSuiteID);
					System.out.println(testCaseID);
					System.out.println(runMode);
					System.out.println(testType);
					System.out.println(browser);

					lifecycle.updateTestCase(testResult -> testResult.setName(testCaseName + "=>" + testCaseID));
					LoggerUtils.info("Changing test case name");

					// Read actions sheet and perform actions
					ractsuite.readTestActionsData(testCaseName.toString().trim(), testCaseID.toString().trim(),
							browser.trim());
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Click here to see Exception Log" + e);

		}
	}

}