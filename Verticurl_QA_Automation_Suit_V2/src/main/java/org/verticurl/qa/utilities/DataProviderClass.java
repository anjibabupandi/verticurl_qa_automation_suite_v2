package org.verticurl.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.verticurl.qa.dependent.TestConfiguration;

public class DataProviderClass {

	public static DataFormatter dataFormatter;
	public static Workbook workbook = null;
	public static FormulaEvaluator formulaEvaluator;
	public static Sheet sheet;
	String actionsPath = null;
	File creatDir = null;
	XLUtils xls;

	// @DataProvider
	public List<Map<String, String>> getTestData() throws Exception {

		workbook = WorkbookFactory.create(new FileInputStream(actionsPath));
		formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
		sheet = workbook.getSheet(TestConfiguration.sh_TestCases);

		int rowNum = sheet.getLastRowNum();
		int colNum = sheet.getRow(0).getLastCellNum();

		System.out.println("rowNum" + rowNum);
		System.out.println("colNum" + colNum);
		List<Map<String, String>> list = new ArrayList();
		Map<String, String> map = null;
		for (int i = 1; i <= rowNum; i++) {
			map = new HashMap<>();
			for (int j = 0; j < colNum; j++) {
				String key = sheet.getRow(0).getCell(j).getStringCellValue();
				String value = sheet.getRow(i).getCell(j).getStringCellValue();

				map.put(key, value);
			}

			list.add(map);
		}
		return list;
	}

	@DataProvider
	public Iterator<Object[]> testCasesExecutor() throws Exception {

		List<Map<String, String>> list = getTestData();
		List<Map<String, String>> list1 = new ArrayList<>();
		Object[] data = new Object[list.size()];
		Collection<Object[]> dp = new ArrayList<Object[]>();
		Map<String, String> map1 = new HashMap<>();
		for (int i = 0; i < list.size(); i++) {

			if (list.get(i).get("RUN_MODE").equals("Y")) {
				list1.add(list.get(i));
			}
		}

		for (Map<String, String> map : list1) {
			dp.add(new Object[] { map });
		}

		System.out.println("data" + list1);

		return dp.iterator();
	}

	public boolean isLocal() throws FileNotFoundException, IOException {
		boolean status = false;
		xls = new XLUtils();
		String runLocation = xls.getCellData(BaseClass.actionsFilePath(), TestConfiguration.sh_TestSuite, 1, 2);
		if (runLocation.equals("Local")) {
			status=true;
		}
		return status;
	}
}
