package org.verticurl.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.verticurl.qa.dependent.TestConfiguration;

public class ReadTestSuite extends BaseClass {
	XLUtils xls;
	List<String> testSuite_TCName;
	List<String> testSuite_TCID;
	List<String> testSuite_Browser;
	List<String> testSuite_TestMethod;

	ReadActionsSuite ractsuite;

	// @DataProvider
	public List<Map<String, String>> getTestData() throws Exception {

		creatDir = new File("C:\\TestData\\Files");
		String[] listOfFiles = creatDir.list();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].equals(TestConfiguration.xlsm_Actions)) {
				actionsPath = creatDir + "\\" + TestConfiguration.xlsm_Actions;
				break;
			} else {
				actionsPath = creatDir + "\\" + TestConfiguration.xlsx_Actions;
			}
		}

		XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(actionsPath));
		XSSFSheet sheet = workbook.getSheetAt(TestConfiguration.TEST_CONFIG);

		int rowNum = sheet.getLastRowNum();
		int colNum = sheet.getRow(0).getLastCellNum();

		System.out.println("rowNum" + rowNum);
		System.out.println("colNum" + colNum);
		List<Map<String, String>> list = new ArrayList();
		Map<String, String> map = null;
		for (int i = 1; i <= rowNum; i++) {
			map = new HashMap<>();
			for (int j = 0; j < colNum; j++) {
				String key = sheet.getRow(0).getCell(j).getStringCellValue();
				String value = sheet.getRow(i).getCell(j).getStringCellValue();
				map.put(key, value);


			}

			list.add(map);
		}
		workbook.close();
		return list;
	}

	@DataProvider
	public Iterator<Object[]> test() throws Exception {

		List<Map<String, String>> list = getTestData();
		List<Map<String, String>> list1 = new ArrayList<>();
		Object[] data = new Object[list.size()];
		Collection<Object[]> dp = new ArrayList<Object[]>();
		Map<String, String> map1 = new HashMap<>();
		for (int i = 0; i < list.size(); i++) {

			if (list.get(i).get("SUITE_TYPE").equals("Y")) {
				list1.add(list.get(i));
			}
		}

		for (Map<String, String> map : list1) {
			dp.add(new Object[] { map });
		}

		System.out.println("data" + data);

		return dp.iterator();
	}

	@Test(dataProvider = "test")
	public void readTestSuiteData(Map<String, String> map1) throws Exception {

		try {
			// System.out.println("data"+map1.size());
			System.out.println("TEST_SUITE_ID" + map1.get("TEST_SUITE_ID"));
			System.out.println("TEST_METHOD_NAME" + map1.get("TEST_METHOD_NAME"));
			System.out.println("SUITE_TYPE" + map1.get("SUITE_TYPE"));
		} catch (Exception e) {
			new SkipException("Data Provider is null or empty!!!!!!!!!!");
		}
		
			/*System.out.println("TEST_SUITE_ID"+map1.get("TEST_SUITE_ID"));
			System.out.println("TEST_CASE_NAME"+map1.get("TEST_METHOD_NAME"));
			System.out.println("TEST_METHOD_NAME"+map1.get("Suite_Type"));
			*/
		
		/*try {
			xls = new XLUtils();
			ractsuite=new ReadActionsSuite();

			testSuite_TCName = new ArrayList<String>();
			testSuite_TCID = new ArrayList<String>();
			testSuite_Browser = new ArrayList<String>();
			testSuite_TestMethod = new ArrayList<String>();
			String actionaFilePath=TestConfiguration.path_Actions;
			
			String cellValue_TCID = null;
			String cellValue_TCName = null;
			String cellValue_TestMthod = null;
			String cellValue_Browser = null;
			
			System.out.println("actionaFilePath" + actionaFilePath);

			DataFormatter dataFormatter = new DataFormatter();
			Workbook workbook = WorkbookFactory.create(new FileInputStream(actionaFilePath));
			FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
			Sheet sheet = workbook.getSheetAt(TestConfiguration.TEST_SUITE);

			for (Row row : sheet) { // iterate over all rows in the sheet
				Cell act_TestSuite_TCID = row.getCell(TestConfiguration.act_TestSuite_TSID);
				Cell act_TestSuite_TCName = row.getCell(TestConfiguration.act_TestSuite_TCName); 
				Cell act_TestSuite_Browser = row.getCell(TestConfiguration.act_TestSuite_Browser);
				Cell act_TestSuite_TestMethod = row.getCell(TestConfiguration.act_TestSuite_TestMethod);

				if (act_TestSuite_TCName != null && act_TestSuite_TCID != null && act_TestSuite_Browser != null) {
					cellValue_TCID = dataFormatter.formatCellValue(act_TestSuite_TCID, formulaEvaluator);
					cellValue_TCName = dataFormatter.formatCellValue(act_TestSuite_TCName, formulaEvaluator);
					cellValue_Browser = dataFormatter.formatCellValue(act_TestSuite_Browser, formulaEvaluator);
					cellValue_TestMthod = dataFormatter.formatCellValue(act_TestSuite_TestMethod,
							formulaEvaluator);

					testSuite_TCID.add(cellValue_TCID); // add that row to the results
					testSuite_TCName.add(cellValue_TCName); // add that row to the results
					testSuite_Browser.add(cellValue_Browser); // add that row to the results
					testSuite_TestMethod.add(cellValue_TestMthod); // add that row to the results
					System.out.println("cellValue_TCID\t==>" + cellValue_TCID);
					System.out.println("cellValue_TCName\t==>" + cellValue_TCName);
					System.out.println("cellValue_TestMthod\t==>" + cellValue_TestMthod);
					System.out.println("cellValue_Browser\t==>" + cellValue_Browser);
				}

			}


			for (int i = 1; i < testSuite_TCID.size(); i++) {
				if (testSuite_TCID.get(i) != null && testSuite_TCName.get(i) != null
						&& testSuite_TestMethod.get(i) != null && testSuite_Browser.get(i) != null) {
					cellValue_TCID = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testSuite_TCID.get(i).toString(), TestConfiguration.act_TestSuite_TSID);
					cellValue_TCName = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testSuite_TCName.get(i).toString(), TestConfiguration.act_TestSuite_TCName);
					cellValue_TestMthod = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testSuite_TestMethod.get(i).toString(), TestConfiguration.act_TestSuite_TestMethod);
					cellValue_Browser = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testSuite_Browser.get(i).toString(), TestConfiguration.act_TestSuite_Browser);

					System.out.println("cellValue_TCID\t==>" + cellValue_TCID);
					System.out.println("cellValue_TCName\t==>" + cellValue_TCName);
					System.out.println("cellValue_TestMthod\t==>" + cellValue_TestMthod);
					System.out.println("cellValue_Browser\t==>" + cellValue_Browser);
					
					//ractsuite.readTestActionsData(cellValue_TestMthod,actionaFilePath,cellValue_TCID);
				}

			}

			workbook.close();
		} catch (Exception e) {

			System.out.println("Exception is" + e.getMessage());
		}*/
	}
}