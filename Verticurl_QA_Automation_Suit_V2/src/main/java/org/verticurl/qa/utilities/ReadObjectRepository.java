package org.verticurl.qa.utilities;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.logger.LoggerUtils;

public class ReadObjectRepository {

	XLUtils xls;
	List<String> orElementName;
	List<String> orElememntLocator;

	public String readObejectRepoData(String objectRepoFilePath, String actionsSheet, String orEleName)
			throws Exception {
		LoggerUtils.info("readObejectRepoData method STARTED");

		String or_EleName = null;
		String or_EleLocator = null;
		// try {

		LoggerUtils.info("actionsSheet" + actionsSheet);
		LoggerUtils.info("orEleName" + orEleName);

		xls = new XLUtils();
		orElementName = new ArrayList<String>();
		orElememntLocator = new ArrayList<String>();
		LoggerUtils.info("objectRepoFilePath" + objectRepoFilePath);

		DataFormatter dataFormatter = new DataFormatter();
		Workbook workbook = WorkbookFactory.create(new FileInputStream(objectRepoFilePath));
		FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
		Sheet sheet = workbook.getSheet(actionsSheet.trim());

		for (Row row : sheet) { // iterate over all rows in the sheet
			Cell or_Ele_Name = row.getCell(TestConfiguration.oR_ELEName);
			Cell or_Ele_Locator = row.getCell(TestConfiguration.oR_ELELocator);

			if (or_Ele_Name != null && or_Ele_Locator != null) {
				String cellValue_OREleName = dataFormatter.formatCellValue(or_Ele_Name, formulaEvaluator);
				String cellValue_OREleLoc = dataFormatter.formatCellValue(or_Ele_Locator, formulaEvaluator);

				orElementName.add(cellValue_OREleName); // add that row to the results
				orElememntLocator.add(cellValue_OREleLoc); // add that row to the results

			}

		}

		for (int i = 1; i < orElementName.size(); i++) {
			or_EleName = xls.readCellContent(objectRepoFilePath, actionsSheet, orElementName.get(i).toString(),
					TestConfiguration.oR_ELEName);

			if (or_EleName.equalsIgnoreCase(orEleName)) {
				or_EleLocator = xls.readCellContent(objectRepoFilePath, actionsSheet,
						orElememntLocator.get(i).toString(), TestConfiguration.oR_ELELocator);

				LoggerUtils.info("or_EleName" + or_EleName);
				LoggerUtils.info("or_EleLocator" + or_EleLocator);

			}

		}

		orElementName.clear();// add that row to the results
		orElememntLocator.clear(); // add that row to the results
		LoggerUtils.info("readObejectRepoData method END");

		workbook.close();

		return or_EleLocator;

	}

}