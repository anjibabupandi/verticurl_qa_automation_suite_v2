package org.verticurl.qa.documents;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.Test;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.utilities.ReadActionsSuite;
import org.verticurl.qa.utilities.XLUtils;

public class ReadTestCases  {
	XLUtils xls;
	List<String> testCases_TSID;
	List<String> testCases_TCID;
	List<String> testCases_RunMode;
	List<String> testCases_TestType;
	List<String> testCases_Name;

	ReadActionsSuite ractsuite;
	public static final Logger logger = Logger.getLogger(ReadTestCases.class);

	HashMap<String,ArrayList<String>> map;
	
	@Test
	public void readTestCasesData(/*String testSuiteID,String testMethod,String browser*/) throws Exception {
		logger.info("readTestCasesData method STARTED");
		String actionaFilePath = TestConfiguration.xlsm_Actions;
		System.out.println("actionaFilePath\t==>" + actionaFilePath);
		 ractsuite=new ReadActionsSuite();
		/*System.out.println("testSuiteID\t==>" + testSuiteID);
		System.out.println("browser\t==>" + browser);*/
		try {
			xls = new XLUtils();
			ractsuite=new ReadActionsSuite();

			testCases_TSID = new ArrayList<String>();
			testCases_TCID = new ArrayList<String>();
			testCases_RunMode = new ArrayList<String>();
			testCases_TestType = new ArrayList<String>();
			testCases_Name = new ArrayList<String>();

			//System.out.println("actionaFilePath" + actionaFilePath);

			DataFormatter dataFormatter = new DataFormatter();
			Workbook workbook = WorkbookFactory.create(new FileInputStream(actionaFilePath));
			FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
			Sheet sheet = workbook.getSheetAt(TestConfiguration.TEST_CASES);

			for (Row row : sheet) { // iterate over all rows in the sheet
				Cell act_TestCases_TSID = row.getCell(TestConfiguration.act_TestCases_TSID);
				Cell act_TestCases_TCID = row.getCell(TestConfiguration.act_TestCases_TCID); 
				Cell act_TestCases_RunMode = row.getCell(TestConfiguration.act_TestCases_RunMode);
				Cell act_TestCases_TestType = row.getCell(TestConfiguration.act_TestCases_TestType);
				Cell act_testCases_Name = row.getCell(TestConfiguration.act_TestCases_TCName);

			/*	System.out.println("act_TestCases_TSID\t==>" + act_TestCases_TSID);
				System.out.println("act_TestCases_TCID\t==>" + act_TestCases_TCID);
				System.out.println("act_TestCases_RunMode\t==>" + act_TestCases_RunMode);
				System.out.println("act_TestCases_TestType\t==>" + act_TestCases_TestType);
				System.out.println("act_testCases_Name\t==>" + act_testCases_Name);*/


				if (act_TestCases_TSID!=null && act_TestCases_TCID != null && act_TestCases_RunMode != null && act_TestCases_TestType!=null) {
					String cellValue_TSID = dataFormatter.formatCellValue(act_TestCases_TSID, formulaEvaluator);
					String cellValue_TCID = dataFormatter.formatCellValue(act_TestCases_TCID, formulaEvaluator);
					String cellValue_RunMode = dataFormatter.formatCellValue(act_TestCases_RunMode, formulaEvaluator);
					String cellValue_TestType = dataFormatter.formatCellValue(act_TestCases_TestType,
							formulaEvaluator);
					String cellValue_TestCaseName = dataFormatter.formatCellValue(act_testCases_Name,
							formulaEvaluator);
					

					
					/*System.out.println("cellValue_TSID\t==>" + cellValue_TSID);
					System.out.println("cellValue_TCID\t==>" + cellValue_TCID);
					System.out.println("cellValue_RunMode\t==>" + cellValue_RunMode);
					System.out.println("cellValue_TestType\t==>" + cellValue_TestType);
					System.out.println("cellValue_TestCaseName\t==>" + cellValue_TestCaseName);*/

					
					testCases_TSID.add(cellValue_TSID); // add that row to the results
					testCases_TCID.add(cellValue_TCID); // add that row to the results
					testCases_RunMode.add(cellValue_RunMode); // add that row to the results
					testCases_TestType.add(cellValue_TestType); // add that row to the results
					testCases_Name.add(cellValue_TestCaseName); // add that row to the results

				}

			}

			String cellValue_TSID = null;
			String cellValue_TCID = null;
			String cellValue_RumMode = null;
			String cellValue_TestType = null;
			String cellValue_TestCaseName = null;
			
			
			
			String cellValue_TSID_Head = null;
			String cellValue_TCID_Head = null;
			String cellValue_RumMode_Head = null;
			String cellValue_TestType_Head = null;
			String cellValue_TestCaseName_Head = null;

            //test
			for (int i = 1; i < testCases_TCID.size(); i++) {
					cellValue_TSID = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TCID.get(i).toString(), TestConfiguration.act_TestCases_TSID);
					cellValue_TCID = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TCID.get(i).toString(), TestConfiguration.act_TestCases_TCID);
					cellValue_RumMode = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_RunMode.get(i).toString(), TestConfiguration.act_TestCases_RunMode);
					cellValue_TestType = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TestType.get(i).toString(), TestConfiguration.act_TestCases_TestType);
					cellValue_TestCaseName = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_Name.get(i).toString(), TestConfiguration.act_TestCases_TCName);
					
		/*cellValue_TSID = testCases_TCID.get(i).toString();
		cellValue_TCID =testCases_TCID.get(i).toString();
		cellValue_RumMode = testCases_RunMode.get(i).toString();
		cellValue_TestType = testCases_TestType.get(i).toString();
		cellValue_TestCaseName=testCases_Name.get(i).toString();
		*/
		/*System.out.println("cellValue_TSID\t==>" + cellValue_TSID);
		System.out.println("cellValue_TCID\t==>" + cellValue_TCID);
		System.out.println("cellValue_RumMode\t==>" + cellValue_RumMode);
		System.out.println("cellValue_TestType\t==>" + cellValue_TestType);
		System.out.println("cellValue_TestCaseName\t==>" + cellValue_TestCaseName);*/
					
					cellValue_TSID_Head = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TCID.get(0).toString(), TestConfiguration.act_TestCases_TSID);
					cellValue_TCID_Head = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TCID.get(0).toString(), TestConfiguration.act_TestCases_TCID);
					cellValue_RumMode_Head = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_RunMode.get(0).toString(), TestConfiguration.act_TestCases_RunMode);
					cellValue_TestType_Head = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_TestType.get(0).toString(), TestConfiguration.act_TestCases_TestType);
					cellValue_TestCaseName_Head = xls.readCellContent(actionaFilePath, sheet.getSheetName(),
							testCases_Name.get(0).toString(), TestConfiguration.act_TestCases_TCName);
					

					/*System.out.println("cellValue_TSID_Head\t==>" + cellValue_TSID_Head);
					System.out.println("cellValue_TCID_Head\t==>" + cellValue_TCID_Head);
					System.out.println("cellValue_RumMode_Head\t==>" + cellValue_RumMode_Head);
					System.out.println("cellValue_TestType_Head\t==>" + cellValue_TestType_Head);
					System.out.println("cellValue_TestCaseName_Head\t==>" + cellValue_TestCaseName_Head);*/
					
					 map = new    HashMap<>();
					if (cellValue_RumMode.equals("Y")) {
						/*System.out.println("cellValue_TSID\t==>" + cellValue_TSID);
						System.out.println("cellValue_TCID\t==>" + cellValue_TCID);
						System.out.println("cellValue_RumMode\t==>" + cellValue_RumMode);
						System.out.println("cellValue_TestType\t==>" + cellValue_TestType);
						System.out.println("cellValue_TestCaseName_Head\t==>" + cellValue_TestCaseName);

						
						System.out.println("cellValue_TSID_Head\t==>" + cellValue_TSID_Head);
						System.out.println("cellValue_TCID_Head\t==>" + cellValue_TCID_Head);
						System.out.println("cellValue_RumMode_Head\t==>" + cellValue_RumMode_Head);
						System.out.println("cellValue_TestType_Head\t==>" + cellValue_TestType_Head);
						System.out.println("cellValue_TestCaseName_Head\t==>" + cellValue_TestCaseName_Head);*/
						
						
						
						map.put(cellValue_TSID_Head, (ArrayList<String>) testCases_TSID);
						map.put(cellValue_TSID_Head, (ArrayList<String>) testCases_TCID);
						map.put(cellValue_RumMode_Head, (ArrayList<String>) testCases_RunMode);
						map.put(cellValue_TestType_Head, (ArrayList<String>) testCases_TestType);
						map.put(cellValue_TestCaseName_Head, (ArrayList<String>) testCases_Name);
						
					//ractsuite.readTestActionsData(testMethod,actionaFilePath,cellValue_TCID);
				}

			}
			System.out.println("map" + map);

			logger.info("readTestCasesData method END");

			workbook.close();
		} catch (Exception e) {

			System.out.println("Exception is" + e.getMessage());
		}
		/*String tc_Name = map.get("TEST_CASE_NAME");
		String ts_ID = map.get("TEST_SUITE_ID");
		String tc_ID = map.get("TEST_CASE_ID");
		String tc_rm = map.get("RUN_MODE");
		String ts_type = map.get("TEST_TYPE");
		String tc_Browser = map.get("BROWSER");

		System.out.println("ts_ID" + ts_ID);
		System.out.println("tc_Name" + tc_Name);
		System.out.println("tc_rm" + tc_rm);
		System.out.println("ts_type" + ts_type);
		ractsuite.readTestActionsData(tc_Name,tc_ID,tc_Browser,tc_rm);*/

	}
	
	
}