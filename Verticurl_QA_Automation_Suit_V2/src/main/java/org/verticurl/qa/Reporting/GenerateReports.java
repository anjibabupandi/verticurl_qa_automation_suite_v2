package org.verticurl.qa.Reporting;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.verticurl.qa.logger.LoggerUtils;
import org.verticurl.qa.utilities.BaseClass;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;

public class GenerateReports implements ITestListener {

	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();

	}

	WebDriver driver;
	BaseClass bs;

	@Attachment(value = "Web Page Screenshot", type = "image/png")
	public byte[] saveFailureScreenShot(WebDriver driver) {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}

	@Attachment(value = "Logs", type = "text/plain")
	public static String saveTextLog(String message) {

		return message;
	}

	@Override
	public void onTestStart(ITestResult iTestResult) {
		try {
			LoggerUtils.info("Execution of onTestStart Method" + getTestMethodName(iTestResult) + "  Started");
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());

		}

	}

	@Override
	public void onTestSuccess(ITestResult iTestResults) {
		try {
			LoggerUtils.info("*** Test execution " + getTestMethodName(iTestResults) + " passed...");

			LoggerUtils.info(iTestResults.getMethod().getMethodName() + " passed!");
			bs = new BaseClass();
			driver = bs.getDriver();
			if (driver instanceof WebDriver) {
				Allure.addAttachment(getTestMethodName(iTestResults) + "Passed",
						new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
			}
			saveTextLog(getTestMethodName(iTestResults) + "Passed and Screenshot taken for Pass");
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}
	}

	@Override
	public void onTestFailure(ITestResult iTestResults) {
		try {
			LoggerUtils.info("*** Test execution " + getTestMethodName(iTestResults) + " failed...");
			LoggerUtils.info(iTestResults.getMethod().getMethodName() + " failed!");

			bs = new BaseClass();
			driver = bs.getDriver();
			if (driver instanceof WebDriver) {
				Allure.addAttachment(getTestMethodName(iTestResults) + "Failed",
						new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
			}
			saveTextLog(getTestMethodName(iTestResults) + "Failed and Screenshot taken for failure");
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		try {
			LoggerUtils.info("execution of onTestSkipped " + getTestMethodName(result) + "  Skipped");
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		try {

			LoggerUtils.info("execution of onTestSkipped " + getTestMethodName(result) + "  Skipped");
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}
	}

	@Override
	public void onStart(ITestContext context) {
		try {

			LoggerUtils.info("Execution of onStart Method" + context.getName());
			context.setAttribute("WebDriver", new BaseClass().getDriver());
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}
	}

	@Override
	public void onFinish(ITestContext context) {
		try {

			LoggerUtils.info("Execution of onFinish Method" + context.getName());
		} catch (FileNotFoundException e) {
			throw new Error("Exception Occured due to " + e.getClass().getName());
		}

	}

}
