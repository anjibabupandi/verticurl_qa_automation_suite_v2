package org.verticurl.qa.api;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.verticurl.qa.utilities.XLUtils;

public class APISuitRunnerClass {
	
	
	public static final String sheetName = ConstVarAPI.apiSheet;
	public static final String path_Actions = ConstVarAPI.path_APIs_File;
	
	XLUtils xls;
	
	public static final String xmlFilePath = "./testngAPI.xml";
	 
   /* public void test() throws IOException {
    	
    	xls=new XLUtils();
    	XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(ConstVarAPI.path_APIs_File));
		XSSFSheet sheet = workbook.getSheet(ConstVarAPI.apiSheet);
		int row = sheet.getLastRowNum();
		int col = sheet.getRow(0).getLastCellNum();
	    String[][] excelData = new String[row][col];

		System.out.println("rowNum" + row);
		System.out.println("colNum" + col);
		List<Map<String,String>> list = new ArrayList();
String valueTest = null;
    	Map<String, String> map = null;
		for (int i = 1; i <=row; i++) {
	        Row rows = sheet.getRow(i);
			for (int j = 0; j < col; j++) {
	            Cell cell = rows.getCell(j);
	            if(cell != null) {
		            excelData[i-1][j] = cell.getStringCellValue();
	            }
	            
			}
		}
    	    sample(transformToMap(excelData), Collections.emptyMap());
}

private static Map<String, List<String>> transformToMap(String[][] data) {
    Map<String, List<String>> map = new HashMap<>();
    for(int i=0;i<data.length;i++) {
    	System.out.println("data["+i+"][0]==="+data[i][0]);
        String key = data[i][0];
        List<String> classes = map.computeIfAbsent(key, k -> new ArrayList<>());
        classes.add(data[i][0]);
    }
    return map;
}
*/
private static void sample() throws IOException {

    //Create an instance on TestNG
    TestNG myTestNG = new TestNG();

    //Create an instance of XML Suite and assign a name for it.
    XmlSuite suite = new XmlSuite();
    suite.setName("Verticurl_API_Automation_Suite");
    //suite.setParameters(parameters);
    suite.addListener("org.verticurl.qa.Reporting.GenerateReports");
        XmlTest xmlTest = new XmlTest(suite);
        xmlTest.setName("API_Automation_Test");
            XmlClass xmlClass = new XmlClass("org.verticurl.qa.api.SendAPIRequest", false);
            xmlTest.getClasses().add(xmlClass);

    //Add the suite to the list of suites.
    List<XmlSuite> suites = new ArrayList<>();
    suites.add(suite);

    System.out.println(suite.toXml());

    //Set the list of Suites to the testNG object you created earlier.
    myTestNG.setXmlSuites(suites);
    
    myTestNG.setOutputDirectory(xmlFilePath);
    
    File file = new File(xmlFilePath);
    System.out.println("file"+file);

    FileWriter writer = new FileWriter(file);
    writer.write(suite.toXml());
    writer.close();

}
public static void main(String args[]) throws IOException, InterruptedException {
	APISuitRunnerClass sr=new APISuitRunnerClass();
	sr.sample();
    TimeUnit.SECONDS.sleep(5);
		// Create object of TestNG Class
		TestNG runner=new TestNG();
		// Create a list of String 
		List<String> suitefiles=new ArrayList<String>();
		// Add xml file which you have to execute
		suitefiles.add("./testngAPI.xml");
		// now set xml file for execution
		runner.setTestSuites(suitefiles);
		// finally execute the runner using run method
		runner.run();
}
}
