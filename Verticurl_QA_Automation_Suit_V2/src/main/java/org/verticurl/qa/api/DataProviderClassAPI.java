package org.verticurl.qa.api;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class DataProviderClassAPI {
	
	
	public List<Map<String, String>> getTestData() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(ConstVarAPI.path_APIs_File));
		XSSFSheet sheet = workbook.getSheet(ConstVarAPI.apiSheet);
		String value = null;
		int rowNum = sheet.getLastRowNum();
		int colNum = sheet.getRow(0).getLastCellNum();

		System.out.println("rowNum" + rowNum);
		System.out.println("colNum" + colNum);
		List<Map<String,String>> list = new ArrayList();
		Map<String, String> map = null;
		for (int i = 1; i <= rowNum; i++) {
			map = new HashMap<>();
			for (int j = 0; j < colNum; j++) {
				String key = sheet.getRow(0).getCell(j).getStringCellValue();
				if(sheet.getRow(i).getCell(j).getCellType()==CellType.STRING) {
					value = sheet.getRow(i).getCell(j).getStringCellValue();
				}
				else if(sheet.getRow(i).getCell(j).getCellType()==CellType.NUMERIC) {
					double getVal = sheet.getRow(i).getCell(j).getNumericCellValue();
					value=String.valueOf((int) getVal);

				}
				map.put(key, value);
				
				System.out.println("key===================================>"+key);
				System.out.println("value=================================>"+value);
			}
			
			list.add(map);
		}
		workbook.close();
		return list;
	}
	
	
@DataProvider
public Iterator<Object[]> testCasesExecutor() throws Exception {
	
	List<Map<String, String>> list=getTestData();
	List<Map<String, String>> list1=new ArrayList<>();
	Collection<Object[]> dp = new ArrayList<Object[]>();
	for (int i = 0; i <list.size(); i++) {
		
		if(list.get(i).get("RUN_MODE").equalsIgnoreCase("Y")) {
			list1.add(list.get(i));
		    }
	}
	
	 for(Map<String,String> map:list1){
	        dp.add(new Object[]{map});
	    }
	
	//System.out.println("data"+data);	

	return dp.iterator();
}
public List<Map<String, String>> getAuthData() throws Exception {
	XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(ConstVarAPI.path_APIs_File));
	XSSFSheet sheet = workbook.getSheet(ConstVarAPI.authSheet);
	String value = null;
	int rowNum = sheet.getLastRowNum();
	int colNum = sheet.getRow(0).getLastCellNum();

	System.out.println("rowNum" + rowNum);
	System.out.println("colNum" + colNum);
	List<Map<String,String>> list = new ArrayList();
	Map<String, String> map = null;
	for (int i = 1; i <= rowNum; i++) {
		map = new HashMap<>();
		for (int j = 0; j < colNum; j++) {
			String key = sheet.getRow(0).getCell(j).getStringCellValue();
			if(sheet.getRow(i).getCell(j).getCellType()==CellType.STRING) {
				value = sheet.getRow(i).getCell(j).getStringCellValue();
			}
			else if(sheet.getRow(i).getCell(j).getCellType()==CellType.NUMERIC) {
				double getVal = sheet.getRow(i).getCell(j).getNumericCellValue();
				value=String.valueOf((int) getVal);;  

			}
			map.put(key, value);
			
			System.out.println("key===================================>"+key);
			System.out.println("value=================================>"+value);
		}
		workbook.close();
		list.add(map);
	}
	return list;
}
@DataProvider
public Iterator<Object[]> getAuth() throws Exception {
	
	List<Map<String, String>> list=getAuthData();
	List<Map<String, String>> list1=new ArrayList<>();
	Collection<Object[]> dp = new ArrayList<Object[]>();
	for (int i = 0; i <list.size(); i++) {
		
		if(list.get(i).get("RUN_MODE").equalsIgnoreCase("Y")) {
			list1.add(list.get(i));
		    }
	}
	 for(Map<String,String> map:list1){
	        dp.add(new Object[]{map});
	    }
	
	//System.out.println("data"+data);	
	return dp.iterator();
}

}
