package org.verticurl.qa.api;

import static org.testng.Assert.assertNotNull;

import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.verticurl.qa.Reporting.GenerateReports;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@Listeners(GenerateReports.class)
public class SendAPIRequest {

	String[] param = null;
	String[] value = null;
	String[] headerName = null;
	String[] headerValue = null;
	String SERVICE_ENDPOINT = null;
	String baseURI, endPoint;
	AllureLifecycle lifecycle = Allure.getLifecycle();

	String paramCell;
	String valueCell;
	String headerNameCell;
	String headerValueCell;
	String statusCode;
	String schemaCell;
	String requestBodyCell;
	String filepath = ConstVarAPI.path_APIs_File;
	ExcelReader exl;
	String accessToken;
	String tokenType;
	String methodName;
	CommonUtility cmUtils;

	@Test(dataProvider = "getAuth", dataProviderClass = DataProviderClassAPI.class)
	public void getAccessToken(Map<String, String> map) throws Exception {
		 lifecycle.updateTestCase(testResult -> testResult.setName(map.get("TEST_CASE_NAME")));

		System.out.println(map.get("SERVICE_BASE_URI"));
		System.out.println(map.get("JOSN_PATH"));
		System.out.println(map.get("STATUS_CODE"));
		System.out.println(map.get("HEADERS_NAME"));
		System.out.println(map.get("HEADERS_VALUE"));
		System.out.println(map.get("REQUEST_BODY"));

		exl = new ExcelReader();
		baseURI = map.get("SERVICE_BASE_URI");
		endPoint = map.get("JOSN_PATH");
		paramCell = map.get("QUERY_PARAM_NAME");
		valueCell = map.get("QUERY_PARAM_VALUE");
		headerNameCell = map.get("HEADERS_NAME");
		headerValueCell = map.get("HEADERS_VALUE");
		statusCode = map.get("STATUS_CODE");
		schemaCell = map.get("RESULT_SCHEMA");
		requestBodyCell = map.get("REQUEST_BODY");

		int expStatusCode = Integer.parseInt(statusCode);
		RestAssured.baseURI = baseURI;

		RequestSpecification request = RestAssured.given();
		System.out.println("-----------------------Request Headers------------");
		request.header("Content-Type", "application/json");

		System.out.println("-----------------------Request Body------------");
		JSONParser parser = new JSONParser();
		JSONObject reqBody = (JSONObject) parser.parse(requestBodyCell);
		System.out.println("reqBody" + reqBody);
		request.body(reqBody);
		Response response = request.request(Method.POST, endPoint);

		System.out.println("response========================" + response.asPrettyString());

		System.out.println("---------------Successfully Printed Request paylod details--------");
		int respStatusCode = response.getStatusCode();

		String json = response.asString();
		JsonPath jp = new JsonPath(json);

		System.out.println("---------------Successfully Printed Access Token--------");
		accessToken = jp.get("accessToken");
		System.out.println(accessToken);

		tokenType = jp.get("token_type");
		int tokenexpires = jp.get("expires_in");
		System.out.println(accessToken);
		System.out.println(tokenType);
		System.out.println(tokenexpires);
		Assert.assertEquals(respStatusCode, expStatusCode, "DEFECT : Status code is coming as different");
		Assert.assertEquals("Bearer", tokenType, "DEFECT : Token Type is coming as different");
		assertNotNull(accessToken, "DEFECT : Accesstoken is null");
		Assert.assertTrue(tokenexpires == 3600 || tokenexpires == 3599,
				"BUG : Expire in is coming as different --- Actual :" + tokenexpires + ":::Expected" + "3600" + "");
		
	}
	int i=1;
	@Test(dataProvider = "testCasesExecutor", dataProviderClass = DataProviderClassAPI.class, dependsOnMethods = "getAccessToken")
	public void getPOSTRequest(Map<String, String> map) throws Exception {
		 lifecycle.updateTestCase(testResult -> testResult.setName(map.get("TEST_CASE_NAME")));

		System.out.println(map.get("SERVICE_BASE_URI"));
		System.out.println(map.get("JOSN_PATH"));
		System.out.println(map.get("STATUS_CODE"));
		System.out.println(map.get("HEADERS_NAME"));
		System.out.println(map.get("HEADERS_VALUE"));
		System.out.println(map.get("REQUEST_BODY"));
		cmUtils=new CommonUtility();
		exl = new ExcelReader();
		methodName = map.get("SERVICE_METHOD");
		baseURI = map.get("SERVICE_BASE_URI");
		endPoint = map.get("JOSN_PATH");
		paramCell = map.get("QUERY_PARAM_NAME");
		valueCell = map.get("QUERY_PARAM_VALUE");
		headerNameCell = map.get("HEADERS_NAME");
		headerValueCell = map.get("HEADERS_VALUE");
		statusCode = map.get("STATUS_CODE");
		schemaCell = map.get("RESULT_SCHEMA");
		requestBodyCell = map.get("REQUEST_BODY");
		JSONObject reqBody = null;
		JSONParser parser = new JSONParser();
		int expStatusCode = Integer.parseInt(statusCode);
		RestAssured.baseURI = baseURI;
		
		RequestSpecification request = RestAssured.given();
		
		System.out.println("-----------------------Request Headers------------");
		System.out.println("Authorization" + accessToken);
		request.header("Content-Type", "application/json");
		request.header("Authorization", tokenType+" "+accessToken);
		//request.param("sync", "xlo,marketo,magento");
		Response response = null;
		
		
		System.out.println("-----------------------Request Body------------");
		if(methodName.equals("POST")) {
		reqBody = (JSONObject) parser.parse(requestBodyCell);
		System.out.println("reqBody" + reqBody);
		request.body(reqBody);
		response = request.request(Method.POST, endPoint);
		}
		else if(methodName.equals("GET")) {
			if(valueCell!=null && valueCell!="NA") {
			response = request.request(Method.GET, endPoint+"/"+valueCell);
			}
			else {
				response = request.request(Method.GET, endPoint);

			}
		}
		
		System.out.println("---------------Successfully Printed Request paylod details--------");
		int respStatusCode = response.getStatusCode();

		String json = response.asString();
		JsonPath jp = new JsonPath(json);
		
		
		exl.setCellData(ConstVarAPI.path_APIs_File, ConstVarAPI.apiSheet, ConstVarAPI.api_Test_Result_Schema, i, json);
		i++;
/*
		JsonPath jsonPathEvaluator = response.jsonPath();
		String email = jsonPathEvaluator.get("email");
		
		String reqJsonEvaluator = reqBody.get("email").toString();


		System.out.println("---------------Respemail--------"+email);
		System.out.println("---------------Reqemail--------"+reqJsonEvaluator);
*/
		System.out.println("---------------Response--------"+json);
		System.out.println("---------------Successfully Printed Access Token--------"+map.get("TEST_CASE_NAME"));
		// System.out.println("accessToken"+jp.get("accessToken"));
		//System.out.println("response========================" + response.asPrettyString());

		Assert.assertEquals(respStatusCode, expStatusCode, "DEFECT : Status code is coming as different");
		
		System.out.println("---------------Request Body from Excel--------"+map.get("REQUEST_BODY"));
		System.out.println("---------------Response Body from Excel--------"+map.get("RESULT_SCHEMA"));
		
		JSONObject respJson=cmUtils.getJosnObject(map.get("RESULT_SCHEMA"));
		
		
        Object obj = new JSONParser().parse(map.get("RESULT_SCHEMA"));
        JSONObject jo = (JSONObject) obj;
        JSONArray ja = (JSONArray) jo.get("updated_magento_rows");

		
		System.out.println("Email=================================>>> "+ja);  
	}
	
}
