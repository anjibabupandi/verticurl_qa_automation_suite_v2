package org.verticurl.qa.api;

public class ConstVarAPI {
	
	private ConstVarAPI() {
		
	}

	// System Variables
	public static final String path_APIs_File = "C:\\TestData\\Files\\TEST_APIs.xlsx";
	public static final String authSheet="AUTH";
	public static final String apiSheet="API";

	
	//API Excel Headers
	public static final int api_Test_Case_Name = 0;
	public static final int api_Test_Service_Name = 1;
	public static final int api_Test_Service_BaseURI = 2;
	public static final int api_Test_Param_Name = 3;
	public static final int api_Test_Param_Value = 4;
	public static final int api_Test_Json_Path = 5;
	public static final int api_Test_Status_Code = 6;
	public static final int api_Test_Header_Name = 7;
	public static final int api_Test_Header_Value = 8;
	public static final int api_Test_Request_Body = 9;
	public static final int api_Test_Result_Schema = 10;
	
	
	//Default Config
	public static final int param = 0;
	public static final int value = 1;


}


