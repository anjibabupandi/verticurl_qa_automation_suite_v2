/**
 * 
 */
package org.verticurl.qa.api;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.verticurl.qa.logger.LoggerUtils;

/**
 * @author Anjibabu-VIN1039
 *
 */
public class ExcelReader {

	private FileInputStream fis;
	private FileOutputStream fos;
	private Workbook wb;
	private Sheet sh;
	private Cell cell;
	private Row row;

	public int getRowCount(String xlFile, String xlSheet) throws IOException {

		int rowCount = 0;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			rowCount = sh.getLastRowNum();

			wb.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return rowCount;

	}

	public int getCellCount(String xlFile, String xlSheet, int rowNum) throws IOException {

		int cellCount = 0;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			row = sh.getRow(rowNum);

			cellCount = row.getLastCellNum();

			wb.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return cellCount;

	}

	public String getCellData(String xlFile, String xlSheet, int rowNum, int colNum) throws IOException {
		String cellData = null;
		try {
			fis = new FileInputStream(xlFile);
			wb = new XSSFWorkbook(fis);
			sh = wb.getSheet(xlSheet);
			row = sh.getRow(rowNum);
			cell = row.getCell(colNum);

			DataFormatter formatter = new DataFormatter();
			cellData = formatter.formatCellValue(cell);
			return cellData;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			cellData = "";
		}

		wb.close();
		fis.close();

		return cellData;

	}

	public void setCellData(String xlFile, String xlSheet, int rowNum, int colNum, String data) throws IOException {
		LoggerUtils.info("xlFile" + xlFile);
		LoggerUtils.info("xlSheet" + xlSheet);
		LoggerUtils.info("rowNum" + rowNum);
		LoggerUtils.info("colNum" + colNum);
		LoggerUtils.info("data" + data);

		fis = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fis);
		sh = wb.getSheet(xlSheet);
		
		row = sh.getRow(colNum);
		LoggerUtils.info("ROW===" + row.getRowNum());

		cell = row.getCell(rowNum);
		LoggerUtils.info("Cell===" + cell);

		cell.setCellValue(data);
		LoggerUtils.info("Cell" + cell.getStringCellValue());

		fis.close();
		fos = new FileOutputStream(xlFile);
		wb.write(fos);
		fos.close();

	}
public static void main(String...args) throws IOException {
	
	ExcelReader e=new ExcelReader();
	e.setCellData(ConstVarAPI.path_APIs_File, ConstVarAPI.apiSheet, 10, 1, "data");
}
}
