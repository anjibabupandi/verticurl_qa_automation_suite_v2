package org.verticurl.qa.dependent;

public class TestConfiguration {

	private TestConfiguration() {

	}

	// System Variables
	String path = "";
	public static final String xlsx_Actions = "TEST_ACTIONS.xlsx";
	public static final String xlsm_Actions = "TEST_ACTIONS.xlsm";

	public static final String path_OR = "OBJECT_REPOSITORY.xlsx";

	// ACTIONS BASIC SHEETS
	public static final int TEST_CONFIG = 0;
	public static final int TEST_CASES = 1;

	public static final String sh_TestSuite = "TEST_CONFIG";
	public static final String sh_TestCases = "TEST_CASES";

	// Data Sheet Column Numbers
	public static final int col_TestCaseID = 0;
	public static final int col_TestDesc = 1;
	public static final int col_TestMthod = 2;
	public static final int col_TestExecute = 3;
	public static final int col_TestType = 4;

	// OR Sheet Column Numbers
	public static final int oR_ELEName = 0;
	public static final int oR_ELELocator = 1;

	// Action Engine Excel sheets

	// TEST_SUITE
	public static final int act_TestSuite_TSID = 0;
	public static final int act_TestSuite_TCName = 1;
	public static final int act_TestSuite_TestMethod = 2;
	public static final int act_TestSuite_Browser = 3;

	// TEST_CASES
	public static final int act_TestCases_TCName = 0;
	public static final int act_TestCases_TSID = 1;
	public static final int act_TestCases_TCID = 2;
	public static final int act_TestCases_RunMode = 3;
	public static final int act_TestCases_TestType = 4;

	// TEST_ACTIONS
	public static final int act_Actions_TCID = 0;
	public static final int act_Actions_TCStep = 1;
	public static final int act_Actions_ORRef = 2;
	public static final int act_Actions_OREle = 3;
	public static final int act_Actions_ActionName = 4;
	public static final int act_Actions_ActionData = 5;

	//Lambda Test Configurations
	public static final String username = "anjibabu.pandi";
	public static final String accessKey = "qlHlcwGxhQsYryRYMrrbkYUdtfrLf5li2dnvZ59ytqn7nRsX3h";
	public static final String gridURLWeb = "@hub.lambdatest.com/wd/hub";
	public static final String gridURLMobile = "@mobile-hub.lambdatest.com/wd/hub";

}
