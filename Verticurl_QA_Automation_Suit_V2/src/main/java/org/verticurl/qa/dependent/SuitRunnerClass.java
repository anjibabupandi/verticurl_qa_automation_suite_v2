package org.verticurl.qa.dependent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.verticurl.qa.logger.LoggerUtils;
import org.verticurl.qa.utilities.BaseClass;
import org.verticurl.qa.utilities.XLUtils;

public class SuitRunnerClass {

	public static final String sheetName = TestConfiguration.sh_TestCases;
	public static final String sheetName_Config = TestConfiguration.sh_TestSuite;
	public static final String xmlFilePath = "./testng.xml";

	XLUtils xls;
	TestNG myTestNG;
	XmlSuite mySuite;
	XmlTest myTest;

	public void runTestNGTest(List testngParams) throws IOException { // Create an instance on TestNG
		myTestNG = new TestNG();
		// Create an instance of XML Suite and assign a name for it.
		mySuite = new XmlSuite();
		mySuite.setName("VeritcurlAutomationSuite");
		mySuite.addListener("org.verticurl.qa.Reporting.GenerateReports");
		xls = new XLUtils();
		String parellelMode = xls.getCellData(BaseClass.actionsFilePath(), sheetName_Config, 1, 2);
		if (parellelMode.contains("Y")) {
			mySuite.setParallel(XmlSuite.ParallelMode.METHODS);
		} else {
			mySuite.setParallel(XmlSuite.ParallelMode.NONE);
		}
		// ======================================test1==============================
		System.out.println("testngParams" + testngParams);
		for (int i = 1; i <= testngParams.size(); i++) {
			createXMLTest(testngParams, i);
		}
		// Add the suite to the list of suites.
		List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
		mySuites.add(mySuite);

		// Set the list of Suites to the testNG object you created earlier.
		myTestNG.setXmlSuites(mySuites);
		mySuite.setFileName(xmlFilePath);
		mySuite.setThreadCount(3);
		myTestNG.run();

		// Create physical XML file based on the virtual XML content
		for (XmlSuite suite : mySuites) {
			createXmlFile(suite);
		}
		File file = new File(xmlFilePath);
		System.out.println("file" + file);
		// Print the parameter values

	}

	public void createXMLTest(List testngParams, int i) {
		XmlTest myTest2 = new XmlTest(mySuite);
		myTest2.setName("readTestCasesData - " + testngParams.get(i - 1).toString());

		// Add any parameters that you want to set to the Test.
		myTest2.addParameter("browser", testngParams.get(i - 1).toString());

		// Create a list which can contain the classes that you want to run.
		List<XmlClass> myClasses2 = new ArrayList<XmlClass>();
		myClasses2.add(new XmlClass("org.verticurl.qa.utilities.ReadTestCases"));

		// Assign that to the XmlTest Object created earlier.
		myTest2.setXmlClasses(myClasses2);

		// Create a list of XmlTests and add the Xmltest you created earlier to it.
		List<XmlTest> myTests2 = new ArrayList<XmlTest>();
		myTests2.add(myTest2);
		myTest2.getAllParameters();
	}

	// This method will create an Xml file based on the XmlSuite data
	public void createXmlFile(XmlSuite mSuite) {
		FileWriter writer;
		try {
			writer = new FileWriter(new File(xmlFilePath));
			writer.write(mSuite.toXml());
			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Main Method
	public static void main(String args[]) throws IOException {
		SuitRunnerClass suiteRun = new SuitRunnerClass();
		XLUtils xls = new XLUtils();
		LoggerUtils.info("actionsPath" + BaseClass.actionsFilePath());
		String browser = xls.getCellData(BaseClass.actionsFilePath(), suiteRun.sheetName, 1, 5);
		List<String> browserList = Arrays.asList(browser.split("\\s*,\\s*"));
		suiteRun.runTestNGTest(browserList);
	}
}
