package org.verticurl.qa.dependent;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.verticurl.qa.logger.LoggerUtils;
import org.verticurl.qa.utilities.BaseClass;
import org.verticurl.qa.utilities.XLUtils;

public class KeywordsEngine {

	String path = System.getProperty("user.dir");
	XLUtils xls;

	WebDriver driver;
	static String[] storeValue = new String[123];

	public void enterText(WebDriver driver, String objectName, String testdata) throws Exception {
		boolean flag = false;

		LoggerUtils.info("enterText Keyword started Execution");
		WebElement textField = driver.findElement(this.getObject(objectName));
		explicitWaitVisibility(driver, objectName);
		if (textField.isEnabled()) {
			textField.clear();
			if (testdata.startsWith("var=")) {
				// Enter the data using variable
				textField.sendKeys(storeValue[testdata.replace("var=", "").hashCode()]);
			} else {
				textField.sendKeys(testdata);
				textField.sendKeys(Keys.ENTER);
			}
			flag = true;
		}

		Assert.assertEquals(true, flag);

	}

	public void wait(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("waiting for the element");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60, 1));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(this.getObject(objectName))));
	}

	public void click(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started Click method" + objectName);
		pageLoadTime(driver);
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
		// for (int m = 0; m < 10; m++) {
		if (element != null) {
			element.click();
			// break;
			// }
		}
		// pageLoadTime(driver);
	}

	public String get_currentURL(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method get_currentURL");
		String URL = driver.getCurrentUrl();
		LoggerUtils.info("print URL " + URL);
		return URL;
	}

	public void quitBrowser(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method quitBrowser");
		driver.quit();
	}

	public void implicitWait(WebDriver driver, int waitTime) throws Exception {
		LoggerUtils.info("Started method implecitWait");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(waitTime));
	}

	public void sleep(WebDriver driver, String waitTime) throws Exception {
		LoggerUtils.info("Started method implecitWait");
		Thread.sleep(Integer.parseInt(waitTime));

	}

	public void selectText(WebDriver driver, String objectName, String selectType) throws Exception {
		LoggerUtils.info("Started method selectText");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		String[] select = selectType.split("=");
		String type = select[0];
		String value = select[1];
		switch (type) {
		case "selectByVisibleText":
			new Select(element).selectByVisibleText(value);
			break;
		case "selectByValue":
			new Select(element).selectByValue(value);
			break;
		case "selectByIndex":
			new Select(element).selectByIndex(Integer.parseInt(value));
			break;

		}

	}

	public void alert(WebDriver driver, String value) throws Exception {
		LoggerUtils.info("Started method alert");

		if (value.equals("accept")) {
			driver.switchTo().alert().accept();
		} else if (value.equals("dismiss")) {
			driver.switchTo().alert().dismiss();
		} else {
			Assert.assertEquals(driver.switchTo().alert().getText(), value);
		}

	}

	public void switchToiFrameByName(WebDriver driver, String name) throws Exception {
		LoggerUtils.info("Started method switchToiFrameByName");
		driver.switchTo().frame(name);

	}

	public void switchToiFrameByID(WebDriver driver, String id) throws Exception {
		LoggerUtils.info("Started method switchToiFrameByID");
		int id1 = Integer.parseInt(id);
		driver.switchTo().frame(id1);
	}

	public void switchToiFrameByID(WebDriver driver, int id) throws Exception {
		LoggerUtils.info("Started method switchToiFrameByID");
		driver.switchTo().frame(id);

	}

	public void switchToiFrameByElementName(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method switchToiFrameByElementName");
		WebElement element = driver.findElement(this.getObject(objectName));
		driver.switchTo().frame(element);

	}

	public void switchToParentiFrame(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method switchToParentiFrame");
		driver.switchTo().defaultContent();
	}

	public void explicitWaitIfClickable(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method explicitWaitIfClickable");
		WebElement element = driver.findElement(this.getObject(objectName));

		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100, 1));
			if (element != null)
				wait.until(ExpectedConditions.elementToBeClickable(element));
			else {

				throw new Exception("Element is null");

			}
		} catch (Exception e) {
			throw new Error(e.getCause());
		}
	}

	public void clickJS(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method clickJS");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		try {
			if (element.isEnabled() && element.isDisplayed()) {
				LoggerUtils.info("Clicking on element " + element.getText() + "+with using java script click");
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				LoggerUtils.info("Unable to click on element");
			}
		} catch (Exception e) {
			LoggerUtils.info("Clicking on element with using java script click");
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(40));
			Actions act = new Actions(driver);
			act.click(element).build().perform();

		}
		pageLoadTime(driver);
	}

	public void waitUntillSpinOverlayDisappears(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method udfWaitUntillSpinOverlayDisappears");
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(180, 1));
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
			if (driver.findElements(By.xpath(".//*[contains(@class,'spinner')]")).size() > 0) {
				for (int i = 0; i < driver.findElements(By.xpath(".//*[contains(@class,'spinner')]")).size(); i++) {
					wait.until(ExpectedConditions
							.invisibilityOfElementLocated(By.xpath(".//*[contains(@class,'spinner')]")));
				}
			} else if (driver.findElements(By.xpath(".//*[contains(@class,'load')]")).size() > 0) {
				for (int i = 0; i < driver.findElements(By.xpath(".//*[contains(@class,'load')]")).size(); i++) {
					wait.until(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[contains(@class,'load')]")));
				}
			}

		} catch (Exception e) {

		}

	}

	public void pageLoadTime(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method pageLoadTime");
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(120, 1));
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
		TimeUnit.SECONDS.sleep(2);
	}

	public void explicitWaitVisibility(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method explicitWaitVisibility");
		WebElement element = driver.findElement(this.getObject(objectName));
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(120, 1));
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			LoggerUtils.info("Exception is in explicitWaitVisibility" + e.getMessage());
		}
		LoggerUtils.info("End method explicitWaitVisibility");

	}

	public void openNewTab() {
		// pageLoadTime(driver);

		Keys.chord(Keys.CONTROL, Keys.TAB);
	}

	public String switchWindows(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method switchWindows");
		String parentWindow = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		List<String> windowStrings = new ArrayList<>(handles);
		String index = windowStrings.get(0);
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver.switchTo().window(windowHandle);
			}
		}
		return index;
	}

	public static void uploadFile(WebDriver driver, String fileLocation) throws Exception {
		LoggerUtils.info("Started method uploadFile");

		try {
			TimeUnit.SECONDS.sleep(10);
			setClipboardData(fileLocation);
			Robot robot = new Robot();
			TimeUnit.SECONDS.sleep(10);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	public static void setClipboardData(String string) throws Exception {

		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public void isDisplayed(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method isDisplayed");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		Assert.assertTrue(element.isDisplayed());
		LoggerUtils.info("End method isDisplayed");

	}

	public void enterTextKey(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started method enterTextKey");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		if (element.isEnabled()) {
			if (val.startsWith("var=")) {
				element.sendKeys(storeValue[val.replace("var=", "").hashCode()] + Keys.RETURN);
			} else {
				element.sendKeys(val + Keys.RETURN);
			}
		}
	}

	public void webDriverWait(WebDriver driver, String strExpCondition, WebElement element, int intTimeOut)
			throws Exception {
		LoggerUtils.info("Started method webDriverWait");
		switch (strExpCondition) {
		case "presenceOfElementLocated":

			break;
		case "invisibilityOfElementLocated":
			(new WebDriverWait(driver, Duration.ofSeconds(120, 1)))
					.until(ExpectedConditions.invisibilityOfElementLocated((By) element));
			break;
		}
	}

	public void isSelected(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method isSelected");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		Assert.assertTrue(element.isSelected());
	}

	public void isClickable(WebDriver driver, String objectName, int timeMs) throws Exception {
		LoggerUtils.info("Started method isClickable");

		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeMs, 1));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		Assert.assertTrue(true);
	}

	public void navigateToPage(WebDriver driver, String val) throws Exception {
		TimeUnit.SECONDS.sleep(2);
		LoggerUtils.info("Started method navigateToPage");
		if (val.startsWith("var=")) {
			driver.navigate().to(storeValue[val.replace("var=", "").hashCode()]);
		} else {
			driver.navigate().to(val);
		}
	}

	public void navigateToBack(WebDriver driver) throws Exception {
		TimeUnit.SECONDS.sleep(2);
		LoggerUtils.info("Started method navigateToBack");
		driver.navigate().back();
	}

	public void pageRefresh(WebDriver driver) throws Exception {
		TimeUnit.SECONDS.sleep(2);
		LoggerUtils.info("Started method pageRefresh");
		driver.navigate().refresh();
	}

	public void navigateToForward(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method navigateToForward");
		driver.navigate().forward();
	}

	public String getCurrentUrl(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method getCurrentUrl");
		String text = driver.getCurrentUrl();
		return text;
	}

	public String getPageTitle(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method getPageTitle");
		String Title = driver.getTitle();
		return Title;
	}

	public void assertElement(WebDriver driver, String objectName, String value) throws Exception {
		LoggerUtils.info("Started method assertElement");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		if (element != null || element.isDisplayed()) {
			if (value.startsWith("var=")) {
				Assert.assertEquals(element.getText(), storeValue[value.replace("var=", "").hashCode()]);
			} else {
				Assert.assertEquals(element.getText(), value);
			}
		}

	}

	public void assertBoolean(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started method assertBoolean");
		boolean flag = Boolean.parseBoolean(val);
		boolean act_flag = false;
		if (driver.findElements(this.getObject(objectName)).size() != 0) {
			explicitWaitVisibility(driver, objectName);
			WebElement element = driver.findElement(this.getObject(objectName));
			act_flag = element.isDisplayed();
		}
		Assert.assertEquals(act_flag, flag);
	}

	public void assertUrl(WebDriver driver, String value) throws Exception {
		LoggerUtils.info("Started method assertUrl");
		Assert.assertEquals(driver.getCurrentUrl(), value);
	}

	public void mouseClick(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method mouseClick");
		Actions action = new Actions(driver);
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		action.click(element).build().perform();
	}

	public void mouseHover(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method mouseHover");
		Actions action = new Actions(driver);
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		action.moveToElement(element).build().perform();
	}

	public void clickEnterKey(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method clickEnterKey");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		element.sendKeys(Keys.ENTER);

	}

	public void scrollToView(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method scrollToView");
		WebElement element = driver.findElement(this.getObject(objectName));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
		pageLoadTime(driver);
	}

	public void scrollByElement(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started scrollByElement method");
		String values[] = val.split(",");
		WebElement element = driver.findElement(this.getObject(objectName));

		if (values[0].equals("0")) {
			if (values[1].equals("max")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollTop = arguments[0].offsetHeight",
						element);
			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollTop +=" + values[1], element);
			}
		} else if (values[1].equals("0")) {
			if (values[0].equals("max")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollLeft = arguments[0].offsetWidth",
						element);
			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollLeft +=" + values[0], element);
			}
		}
	}

	public void scrollByWindow(WebDriver driver, String val) throws Exception {
		LoggerUtils.info("Started scrollHorizontalByWindow method");
		String values[] = val.split(",");

		if (values[0].equals("max")) {
			values[0] = "document.body.scrollWidth";
		}
		if (values[1].equals("max")) {
			values[1] = "document.body.scrollHeight";
		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(" + values[0] + "," + values[1] + ")");

	}

	public void clickPAGEUPKey(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method clickPAGEUPKey");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_UP).build().perform();

	}

	public void clickPAGEDOWNKey(WebDriver driver) throws Exception {
		LoggerUtils.info("Started method clickPAGEDOWNKey");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
	}

	public void clickCONTROLKey(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method clickCONTROLKey");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		element.sendKeys(Keys.CONTROL);

	}

	public void clickRETURNKey(WebDriver driver, String objectName) throws Exception {
		LoggerUtils.info("Started method clickRETURNKey");
		explicitWaitVisibility(driver, objectName);
		WebElement element = driver.findElement(this.getObject(objectName));
		element.sendKeys(Keys.RETURN);

	}

	public String getStringValue(String val) throws Exception {

		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String randomString = null;
		StringBuilder sb = new StringBuilder();
		int length = 7;
		for (int i = 0; i < length; i++) {
			int index = new Random().nextInt(alphabet.length());
			char randomChar = alphabet.charAt(index);
			randomString = sb.append(randomChar).toString();
		}
		String str = val.concat(randomString);
		LoggerUtils.info("str-->  " + str);
		return str;
	}

	String strValue;
	InputStream inp;
	Workbook wb;
	Sheet sheet;
	FileOutputStream out;

	public String randomString(WebDriver driver, String val) throws Exception {
		LoggerUtils.info("Started method randomString");

		String values[] = val.split(",");
		String valMail[];
		String valFinal;

		if (values[0].contains("@")) {
			valMail = values[0].split("@");
			valFinal = valMail[0];
			storeValue[values[1].replace("var=", "").hashCode()] = getStringValue(valFinal) + "@" + valMail[1];
		} else {
			valFinal = values[0];
			strValue = getStringValue(valFinal);
			storeValue[values[1].replace("var=", "").hashCode()] = getStringValue(valFinal);

		}
		LoggerUtils.info("valFinal" + valFinal);
		return storeValue[values[1].replace("var=", "").hashCode()];

	}

	public String randomNumber(WebDriver driver, String val) throws Exception {
		LoggerUtils.info("Started method randomNumber");
		String values[] = val.split(",");
		int randomNumber = (int) (Math.random() * (99999999 - 10000000 + 1) + 10000000);
		storeValue[values[1].replace("var=", "").hashCode()] = values[0] + randomNumber;
		LoggerUtils.info("Final randomNumber is : " + storeValue[values[1].replace("var=", "").hashCode()]);
		return storeValue[values[1].replace("var=", "").hashCode()];

	}

	/*
	 * public String randomString(WebDriver driver, String val) throws Exception {
	 * LoggerUtils.info("Started method randomString");
	 * 
	 * xls = new XLUtils();
	 * 
	 * String valMail[]; String valFinal;
	 * 
	 * if (val.contains("@")) { valMail = val.split("@"); valFinal = valMail[0];
	 * strValue = getStringValue(valFinal) + "@" + valMail[1]; } else { valFinal =
	 * val; strValue = getStringValue(valFinal);
	 * 
	 * }
	 * 
	 * LoggerUtils.info("valFinal" + valFinal); sheet = ReadActionsSuite.sheet; wb =
	 * ReadActionsSuite.workbook; LoggerUtils.info("Current Sheet Name" +
	 * sheet.getSheetName()); int columnNumber = xls.searchStringInXslx(val,
	 * filepath, sheet.getSheetName()); LoggerUtils.info("columnNumber Value is" +
	 * columnNumber);
	 * 
	 * int newValue = columnNumber + 1; LoggerUtils.info("newValue Value is" +
	 * newValue);
	 * 
	 * if (strValue instanceof String) {
	 * sheet.getRow(newValue).createCell(ConstVar.act_Actions_ActionData).
	 * setCellValue((String) strValue); } out = new FileOutputStream(filepath);
	 * wb.write(out); TimeUnit.SECONDS.sleep(3); out.close(); return strValue;
	 * 
	 * }
	 * 
	 * public String randomStringValidation(WebDriver driver) throws Exception {
	 * sheet = ReadActionsSuite.sheet; wb = ReadActionsSuite.workbook;
	 * 
	 * LoggerUtils.info("shindex" + sheet.getSheetName()); int columnNumber =
	 * xls.searchStringInXslx("randomStringValidation", filepath,
	 * sheet.getSheetName()); LoggerUtils.info("columnNumber Value is" +
	 * columnNumber);
	 * 
	 * int newValue = columnNumber + 1; LoggerUtils.info("newValue Value is" +
	 * newValue);
	 * 
	 * if (strValue instanceof String) {
	 * sheet.getRow(newValue).createCell(ConstVar.act_Actions_ActionData).
	 * setCellValue((String) strValue); } out = new FileOutputStream(filepath);
	 * wb.write(out); TimeUnit.SECONDS.sleep(3);
	 * 
	 * out.close(); return strValue;
	 * 
	 * }
	 */

	String index = null;

	public String openNewWindow(WebDriver driver, String val) throws Exception {
		pageLoadTime(driver);
		String parentWindow = driver.getWindowHandle();
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		index = switchWindows(driver);
		driver.navigate().to(val);
		pageLoadTime(driver);
		LoggerUtils.info(driver.getTitle());
		LoggerUtils.info("index" + index);
		return index;

	}

	public void switchToDefaultBrowser(WebDriver driver, String indexVal) throws Exception {
		LoggerUtils.info("indexVal" + indexVal);
		pageLoadTime(driver);
		Set<String> handles = driver.getWindowHandles();
		List<String> windowStrings = new ArrayList<>(handles);
		int indVal = Integer.parseInt(indexVal);
		index = windowStrings.get(indVal);
		LoggerUtils.info("index value " + index);

		driver.switchTo().window(index);
	}

	public void dragAndDropAction(WebDriver driver, String object) throws Exception {
		String[] locatorIdentifier = object.split("[;]", 2);

		LoggerUtils.info("locatorIdentifier[1]" + locatorIdentifier[1]);
		LoggerUtils.info("locatorIdentifier[0]" + locatorIdentifier[0]);
		Actions action = new Actions(driver);
		WebElement source = driver.findElement(this.getObject(locatorIdentifier[0]));
		WebElement destination = driver.findElement(this.getObject(locatorIdentifier[1]));

		action.dragAndDrop(source, destination).build().perform();

	}

	/*
	 * public String getLinkAndNavigate(WebDriver driver, String objectName, String
	 * val) throws Exception { String link = null; String tagName=null; String
	 * finalValue = null; if (driver.findElements(this.getObject(objectName)).size()
	 * != 0) { WebElement element = driver.findElement(this.getObject(objectName));
	 * tagName=element.getTagName(); if (tagName.equalsIgnoreCase("a")) { link =
	 * element.getAttribute("href"); } else { link = element.getAttribute("value");
	 * }
	 * 
	 * LoggerUtils.info("Link is" + link); } if (!(link == null && val == null)) {
	 * finalValue = link + val; } else { finalValue = link; }
	 * LoggerUtils.info("finalValue link is" + finalValue);
	 * driver.navigate().to(finalValue); pageLoadTime(driver);
	 * 
	 * return link; }
	 */

	public void getValue(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started getValue method");
		WebElement element = driver.findElement(this.getObject(objectName));
		storeValue[val.replace("var=", "").hashCode()] = element.getAttribute("value");
		LoggerUtils.info("getValue is" + storeValue[val.replace("var=", "").hashCode()]);
	}

	public void getText(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started getText method");
		WebElement element = driver.findElement(this.getObject(objectName));
		storeValue[val.replace("var=", "").hashCode()] = element.getText();
		LoggerUtils.info("getText is" + storeValue[val.replace("var=", "").hashCode()]);
	}

	public void getHref(WebDriver driver, String objectName, String val) throws Exception {
		LoggerUtils.info("Started getHref method");
		WebElement element = driver.findElement(this.getObject(objectName));
		storeValue[val.replace("var=", "").hashCode()] = element.getAttribute("href");
		LoggerUtils.info("getHref is" + storeValue[val.replace("var=", "").hashCode()]);
	}

	public String append(WebDriver driver, String val) throws Exception {
		String values[] = val.split(",");
		String sv = storeValue[values[values.length - 1].replace("var=", "").hashCode()];
		sv = "";
		sv = sv == null ? "" : sv;

		for (int k = 0; k < values.length - 1; k++) {
			if (values[k].toString().startsWith("var=")) {
				sv = sv + storeValue[values[k].replace("var=", "").hashCode()];
			} else {
				sv = sv + values[k];
			}
		}
		storeValue[values[values.length - 1].replace("var=", "").hashCode()] = sv;
		LoggerUtils.info("finalValue is " + storeValue[values[values.length - 1].replace("var=", "").hashCode()]);
		return storeValue[values[values.length - 1].replace("var=", "").hashCode()];
	}

	public By getObject(String element) throws Exception {
		LoggerUtils.info("Object_element" + element);

		String[] locatorIdentifier = element.split("=", 2);

		LoggerUtils.info("locatorIdentifier[0]" + locatorIdentifier[0]);
		LoggerUtils.info("locatorIdentifier[1]" + locatorIdentifier[1]);

		// find by xpath
		if (locatorIdentifier[0].equalsIgnoreCase("xpath")) {
			LoggerUtils.info("locatorIdentifier=>xpath" + By.xpath("" + locatorIdentifier[1] + ""));

			return By.xpath("" + locatorIdentifier[1] + "");
		}
		// find by id
		else if (locatorIdentifier[0].equalsIgnoreCase("id")) {
			LoggerUtils.info("locatorIdentifier=>id" + By.id("" + locatorIdentifier[1] + ""));

			return By.id("" + locatorIdentifier[1] + "");

		}
		// find by class
		else if (locatorIdentifier[0].equalsIgnoreCase("className")) {
			LoggerUtils.info("locatorIdentifier=>className" + By.className("" + locatorIdentifier[1] + ""));

			return By.className("" + locatorIdentifier[1] + "");

		}
		// find by name
		else if (locatorIdentifier[0].equalsIgnoreCase("name")) {
			LoggerUtils.info("locatorIdentifier=>name" + By.name("" + locatorIdentifier[1] + ""));

			return By.name("" + locatorIdentifier[1] + "");

		}
		// Find by css
		else if (locatorIdentifier[0].equalsIgnoreCase("css")) {
			LoggerUtils.info("locatorIdentifier=>cssSelector" + By.cssSelector("" + locatorIdentifier[1] + ""));

			return By.cssSelector("" + locatorIdentifier[1] + "");

		}
		// find by link
		else if (locatorIdentifier[0].equalsIgnoreCase("linkText")) {
			LoggerUtils.info("locatorIdentifier=>linkText" + By.linkText("" + locatorIdentifier[1] + ""));

			return By.linkText("" + locatorIdentifier[1] + "");

		}
		// find by partial link
		else if (locatorIdentifier[0].equalsIgnoreCase("partialLinkText")) {
			System.out
					.println("locatorIdentifier=>partialLinkText" + By.partialLinkText("" + locatorIdentifier[1] + ""));

			return By.partialLinkText("" + locatorIdentifier[1] + "");

		} else if (locatorIdentifier[0].equalsIgnoreCase("tagName")) {
			System.out
					.println("locatorIdentifier=>partialLinkText" + By.partialLinkText("" + locatorIdentifier[1] + ""));

			return By.tagName("" + locatorIdentifier[1] + "");

		}
		return null;

	}

}
