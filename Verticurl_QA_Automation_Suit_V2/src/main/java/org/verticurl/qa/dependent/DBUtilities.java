package org.verticurl.qa.dependent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.verticurl.qa.utilities.GenericUtilities;

/**
 * @author Anjibabu-VIN1039
 *
 */
public class DBUtilities extends GenericUtilities {

	WebDriver driver;

	public DBUtilities(WebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "input_username")
	@CacheLookup
	WebElement inputUserName;

	@FindBy(id = "input_password")
	@CacheLookup
	WebElement inputUserPass;

	@FindBy(id = "select_server")
	@CacheLookup
	WebElement selectDBServer;

	@FindBy(id = "input_go")
	@CacheLookup
	WebElement clickSubmit;

	@FindBy(xpath = "//body/div[@id='pma_navigation']/div[@id='pma_navigation_content']/div[@id='pma_navigation_tree']/div[@id='pma_navigation_tree_content']/ul[1]/li[8]/div[1]/a[1]/img[1]")
	@CacheLookup
	WebElement clickQADb;

	@FindBy(xpath = ".//a[.='qa']")
	@CacheLookup
	WebElement clickQATable;

	@FindBy(xpath = ".//a[text()='users']")
	@CacheLookup
	WebElement clickUsersTable;

	@FindBy(xpath = ".//input[@class='filter_rows']")
	@CacheLookup
	WebElement searchUser;

	@FindBy(xpath = "//a[.='client_setting']")
	@CacheLookup
	WebElement openClientSettingsTable;

	@FindBy(xpath = "//a[contains(.,'SQL')]")
	@CacheLookup
	WebElement clickSQLTab;

	@FindBy(xpath = "//div[@id='queryfieldscontainer']//textarea[@name='sql_query']")
	@CacheLookup
	WebElement sqlEditor;

	@FindBy(id = "button_submit_query")
	@CacheLookup
	WebElement submitQuery;

	@FindBy(xpath = "//td[@data-type='int']//span")
	@CacheLookup
	WebElement getIntTypeValue;

	@FindBy(xpath = "//td[@data-type='string']//span")
	@CacheLookup
	WebElement getStringTypeValue;

	/**
	 * 
	 * @param driver
	 * @param userName
	 * @param pass
	 * @param serverName
	 * @return
	 * @throws Exception
	 * 
	 *                   This method is used to perform login to data base
	 */
	public boolean loginToXloDB(WebDriver driver, String userName, String pass, String serverName) throws Exception {
		boolean flag = false;
		explicitWaitVisibility(driver, inputUserName);
		enterText(inputUserName, userName);
		enterText(inputUserPass, pass);
		// selectText(selectDBServer, "selectByVisibleText", serverName);
		if (clickSubmit.isEnabled()) {
			click(clickSubmit);
			pageLoadTime(driver);
			pageLoadTime5Seconds(driver);
			flag = true;
		}
		return flag;

	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws Exception
	 * 
	 *                   this is verifying users table in DB
	 * 
	 */
	public boolean verifyUsersTable(WebDriver driver) throws Exception {
		boolean flag = false;
		explicitWaitVisibility(driver, clickUsersTable);
		if (clickUsersTable.isDisplayed()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickUsersTable);
			pageLoadTime(driver);
			pageLoadTime5Seconds(driver);
			flag = true;
		}
		return flag;

	}

	/**
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void navigateToQATable(WebDriver driver) throws Exception {
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		explicitWaitVisibility(driver, clickQADb);

		Actions actions = new Actions(driver);
		actions.moveToElement(clickQADb).build().perform();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickQADb);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		explicitWaitVisibility(driver, clickQATable);
		actions.moveToElement(clickQATable).build().perform();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickQATable);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws Exception
	 */
	public String getUserIDfromUsersTable(WebDriver driver) throws Exception {
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		String userID = null;
		if (getIntTypeValue.isDisplayed()) {
			userID = getIntTypeValue.getText();
		}
		return userID;

	}

	public String getClientDbNameFromClientSettingsTable(WebDriver driver) throws Exception {
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		String userID = null;
		if (getStringTypeValue.isDisplayed()) {
			userID = getStringTypeValue.getText();
		}
		return userID;

	}

	/**
	 * 
	 * @param driver
	 * @param sqlQuery
	 * @throws Exception
	 * 
	 *                   Validating data from given table
	 */
	public void getDatafromTable(WebDriver driver, String sqlQuery) throws Exception {
		Actions actions = new Actions(driver);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickSQLTab);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		actions.moveToElement(sqlEditor).click().build().perform();
		sqlEditor.clear();
		sqlEditor.sendKeys(sqlQuery);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", submitQuery);
		pageLoadTime(driver);
		pageLoadTime5Seconds(driver);
	}

}
