package org.verticurl.qa.dependent;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;

public class KeyWordExecution {
	public void runReflectionMethod(String strClassName, String strMethodName, Object... inputArgs) {

		Class<?> params[] = new Class[inputArgs.length];

		for (int i = 0; i < inputArgs.length; i++) {
			if (inputArgs[i] instanceof String) {
				params[i] = String.class;
			} else if (inputArgs[i] instanceof Integer) {
				params[i] = Integer.class;

			} else if (inputArgs[i] instanceof WebDriver) {
				params[i] = WebDriver.class;

			}
		}
		try {
			Class<?> cls = Class.forName(strClassName);
			Object _instance = cls.newInstance();
			Method myMethod = cls.getDeclaredMethod(strMethodName, params);
			myMethod.invoke(_instance, inputArgs);

		} catch (Exception e) {
			System.err.format(strClassName + ":- Class not found%n");
		}
	}

}