package org.verticurl.mobile.dependent;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.driverUtility.CapabilitySupplier;

public class MobileBrowserFactory {
	public static WebDriver driver = null;

	public static WebDriver startApplicationMobile(String browser) throws FileNotFoundException {
		// driver open
		if (browser.equalsIgnoreCase("android")) {

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("LT:Options", CapabilitySupplier.mobileCapabalitySet(browser));

			try {
				driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
						+ TestConfiguration.accessKey + TestConfiguration.gridURLMobile), capabilities);
			} catch (MalformedURLException e) {
				System.out.println("Invalid grid URL");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			return driver;
		}

		if (browser.equalsIgnoreCase("ios")) {

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("LT:Options", CapabilitySupplier.mobileCapabalitySet(browser));
			try {
				driver = new RemoteWebDriver(new URL("https://" + TestConfiguration.username + ":"
						+ TestConfiguration.accessKey + TestConfiguration.gridURLMobile), capabilities);
			} catch (MalformedURLException e) {
				System.out.println("Invalid grid URL");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			return driver;
		}
		return driver;

	}

}
