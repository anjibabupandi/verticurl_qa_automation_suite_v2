package org.verticurl.mobile.dependent;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.verticurl.qa.utilities.BaseClass;

public class MobileDeviceDetails {

	List<Comparable> deviceDetails;

	public List getDeviceDetails(String deviceType) {
		try {
			FileInputStream file = new FileInputStream(BaseClass.actionsFilePath());
			XSSFWorkbook wb = new XSSFWorkbook(file);
			XSSFSheet sheet = wb.getSheet("DEVICE_CONFIG");
			deviceDetails = new ArrayList<Comparable>();
			int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();

			for (int i = 1; i <= rowCount; i++) {
				// Enter the values read from Excel in firstname,lastname,mobile,email,address
				String deviceName = sheet.getRow(i).getCell(0).getStringCellValue();
				String platformName = sheet.getRow(i).getCell(1).getStringCellValue();
				Double platformVerison = sheet.getRow(i).getCell(2).getNumericCellValue();

				if (platformName.equals(deviceType)) {
					deviceDetails.add(deviceName);
					deviceDetails.add(platformName);
					deviceDetails.add(platformVerison);
					break;
				} else {
					deviceDetails.add(deviceName);
					deviceDetails.add(platformName);
					deviceDetails.add(platformVerison);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return deviceDetails;
	}

}
