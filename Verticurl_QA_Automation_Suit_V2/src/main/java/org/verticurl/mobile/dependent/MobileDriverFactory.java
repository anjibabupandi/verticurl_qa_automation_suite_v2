package org.verticurl.mobile.dependent;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.verticurl.qa.dependent.TestConfiguration;
import org.verticurl.qa.driverUtility.DriverType;

public class MobileDriverFactory {

	
	static DesiredCapabilities capabilities = new DesiredCapabilities();
	MobileDriverFactory() {
		capabilities.setCapability("visual", true);
		capabilities.setCapability("network", true);
		capabilities.setCapability("video", true);
		capabilities.setCapability("build", "VC_Mobile_Build");
		capabilities.setCapability("name", "VC_Mobile_Test");
		capabilities.setCapability("project", "VC_Mobile_Automation");
		capabilities.setCapability("isRealMobile", true);
		capabilities.setCapability("console", true);


	}
	public static String username = TestConfiguration.username;
    public static String accesskey = TestConfiguration.accessKey;
    public static String gridURL = "@mobile-hub.lambdatest.com/wd/hub";
    public static String remoteAddress="https://" + username + ":" + accesskey + gridURL;
    public static RemoteWebDriver driver = null;

    boolean status = false;
	
	private static final Map<DriverType, Supplier<WebDriver>> driverMap = new HashMap<>();

	// chrome driver supplier
	private static final Supplier<WebDriver> androidmobilesupplier = () -> {
    	capabilities.setCapability("platformName", "android");
    	capabilities.setCapability("deviceName", "Galaxy S20");
    	capabilities.setCapability("platformVersion", "10");
    	
	        try {
	            driver = new RemoteWebDriver(new URL(remoteAddress), capabilities);
	        } catch (MalformedURLException me) {
	            System.out.println("Invalid grid URL");
	        } catch (Exception e) {
	            System.out.println(e.getMessage());
	        }

		return driver;
	};

	// firefox driver supplier
	private final static Supplier<WebDriver> iosmobilesupplier = () -> {
		FirefoxOptions options = new FirefoxOptions();
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "ios");
		capabilities.setCapability("deviceName", "iPhone 12 Pro");
		capabilities.setCapability("platformVersion", "16");
		
		 try {
	            driver = new RemoteWebDriver(new URL(remoteAddress), capabilities);
	        } catch (MalformedURLException me) {
	            System.out.println("Invalid grid URL");
	        } catch (Exception e) {
	            System.out.println(e.getMessage());
	        }

		return driver;
	};

	static {
		driverMap.put(DriverType.CHROME, androidmobilesupplier);
		driverMap.put(DriverType.SAFARI, iosmobilesupplier);

	}

	// return a new driver from the map
	public static final WebDriver getDriver(DriverType type) {
		return driverMap.get(type).get();
	}

}
